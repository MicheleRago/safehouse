# SafeHouse
## Descrizione
Il progetto ha come scopo quello di realizzare un sistema di sorveglianza/allarme smart per la casa che possa essere controllato da remoto tramite un'applicazione Android e da un sistema locale, eseguito ad esempio su un raspberry pi.
## Deploy
### Pannello di controllo (raspberry pi)
#### Creazione Access Point
Installare i pacchetti necessari
```sh
sudo apt-get install hostapd dnsmasq -y
```
Modificare il file dhcpcd.conf aggiungendo alla fine del file la riga "**denyinterfaces wlan0**"
```sh
sudo nano /etc/dhcpcd.conf
```
##### Indirizzo IP statico
Impostare un indirizzo IP statico per l'interfaccia WiFi modificando il file **interfaces**
```sh
sudo nano /etc/network/interfaces
```
Aggiungendo il seguente testo:
```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet static
    address 10.0.0.1
    netmask 255.255.255.0
    network 10.0.0.0
    broadcast 10.0.0.255
```
##### Configura Hostapd
Creare il file **hostapd.conf**
```sh
sudo nano /etc/hostapd/hostapd.conf
```
Aggiungendo il seguente testo:
```
interface=wlan0
driver=nl80211
ssid=SafeHouse
hw_mode=g
channel=6
ieee80211n=1
wmm_enabled=1
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_passphrase=safehouse
rsn_pairwise=CCMP
```
Assegnare il file hostapd.conf allo script di avvio di hostapd
```sh
sudo nano /etc/default/hostapd
```
Trovare la riga #DAEMON_CONF="" e sostituiscila con:
```
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```
##### Configura Dnsmasq
Il file .conf fornito con Dnsmasq contiene molte informazioni, quindi potrebbe valere la pena salvarlo (come backup) piuttosto che eliminarlo. Dopo averlo salvato, aprirne uno nuovo per la modifica:
```sh
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.bak
sudo nano /etc/dnsmasq.conf
```
Aggiungendo il seguente testo:
```
interface=wlan0
listen-address=10.0.0.1
bind-interfaces
server=8.8.8.8
domain-needed
bogus-priv
dhcp-range=10.0.0.10,10.0.0.200,24h
```
##### Abilita Packet Forwarding
Modificare il file /etc/sysctl.conf, cerca la riga #net.ipv4.ip_forward=1 e decommentala eliminando #.
```sh
sudo nano /etc/sysctl.conf
```
Configurare la Network Address Translation (NAT) tra le interfacce Ethernet e WiFi per consentire ai dispositivi su entrambe le reti di comunicare tra loro.
```sh
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE  
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
```
Questo ora funzionerà, ma al riavvio, il raspberry pi tornerà al suo stato precedente. Per risolvere questo problema, abbiamo bisogno che queste regole NAT vengano applicate ogni volta che si avvia, bisogna salvare le regole correnti in un file con questo comando:
```sh
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
```
Modificare il file rc.local 
```sh
sudo nano /etc/rc.local
```
Aggiungendo prima di **exit 0** la riga seguente:
```sh
iptables-restore < /etc/iptables.ipv4.nat 
```
#### Docker e containers
##### Installare docker
Lanciare i seguenti comandi:
```sh
sudo apt-get update && sudo apt-get upgrade
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker pi
```
##### Creazione del container postgres
Lanciare il seguente comando:
```sh
docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -p  5432: 5432 --restart always -d postgres
```
##### Creazione del container eclipse-mosquitto
Lanciare il seguente comando:
```sh
sudo mkdir /srv/mosquitto/config /srv/mosquitto/data /srv/mosquitto/log -p
```
Creato il percorso, creiamo un primo file di configurazione:
```sh
sudo nano /srv/mosquitto/config/mosquitto.conf
```
Aggiungendo il seguente testo:
```
persistence true
persistence_location /mosquitto/data/

log_dest file /mosquitto/log/mosquitto.log
log_dest stdout

listener 1883 0.0.0.0

password_file /mosquitto/config/mosquitto.passwd
allow_anonymous false
```
Creare il file contenente l'utente e la sua password rispettivamente pippo e pluto
```sh
sudo nano /srv/mosquitto/config/mosquitto.passwd
```
Aggiungendo il seguente testo:
```sh
pippo:$7$101$4MEuPODv8waxbOxL$3O03EqWeoTYcu+7ra1Jqu8VJHfJKYw8l/u1CX7V4RzD5xPVmlCLQ4sIqgPufnRB5D1bhxeRaQvLP3wvF1TsrIQ==
```
Creiamo il container con il seguente comando:
```sh
docker run --init -d -it -p 1883:1883 --name mosquitto --restart always -v /srv/mosquitto/config:/mosquitto/config -v /srv/mosquitto/log:/mosquitto/log -v /srv/mosquitto/data/:/mosquitto/data eclipse-mosquitto
```
##### Creazione del container safehouse
Creiamo il container con il seguente comando:
```sh
docker run -it   --device /dev/gpiomem -p 80:8000  --restart always nannuzzi/safehouse:latest 
```