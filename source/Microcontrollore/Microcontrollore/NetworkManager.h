/**
   @file NetworkManager.h

   @brief Serie di metodi per la gestione della rete.
*/
#pragma once
#include <ESP8266WiFi.h>

bool startAP();
bool connectToWiFi(String ssid, String password);
bool connectToWiFi();
int getWiFiStrength();
String getWiFiSSID();
String getWiFiPassword();
bool isConnected();
void getUDID(char* udid);