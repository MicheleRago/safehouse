/**
   @file NetworkManager.h

   @brief Serie di metodi per la gestione della rete.
*/
#pragma once
#include "Arduino.h"

#define sensorsNumber 5
#define pir1 D1
#define pir2 D2
#define pir3 D5
#define pir4 D6
#define pir5 D7

void setupPIRSensorManager();
uint8_t getPIRSensorManagerStatus(int sensorId);