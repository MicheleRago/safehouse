/**
   @file MemoryManger.h

   @brief Serie di metodi utilizzati per gestire il file system.
*/

#pragma once
#include <ArduinoJson.h>
#include <LittleFS.h>

#define SENSORS_FILE_PATH "/sensors.json"

bool setupMemoryManager();
String readFile(String path);
bool writeFile(String path, String input);
bool existFile(String path);
bool removeFile(String path);
StaticJsonDocument<512> getSensorsFile();