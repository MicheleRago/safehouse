/**
   @file MemoryManger.cpp

   @brief Serie di metodi per la gestione della rete.
*/
#include "NetworkManager.h"

String currentSSID, currentPassword;

/**
   Avvia l'access point.
*/
bool startAP() {
    String ssid = "ESP8266-" + String(ESP.getChipId(), HEX);

    WiFi.mode(WIFI_AP);
    // Note: by default, the access point IP address is 192.168.4.1
    if (!WiFi.softAP(ssid)) {
        Serial.println("[ERROR] Failed to create the Access Point");
        return false;
    }
    Serial.println(
        String("The Access Point(" + ssid + ") was created successfully"));
    return true;
}

/**
   Gestisce la connessione al WiFi.

   @param ssid stringa contente ssid delle rete che si vuole creare
   @param password stringa contente la password delle rete che si vuole creare.
*/
bool connectToWiFi(String ssid, String password) {
    WiFi.disconnect();
    if (WiFi.getAutoConnect()) {
        Serial.println(F("Disabling auto-connect"));
        WiFi.setAutoConnect(false);
    }
    WiFi.persistent(false);
    WiFi.mode(WIFI_STA);
    currentSSID = ssid;
    currentPassword = password;
    return connectToWiFi();
}

/**
   Gestisce la connessione al WiFi.
*/
bool connectToWiFi() {
    // connect to WiFi (if not already connected)
    if (WiFi.status() != WL_CONNECTED) {
        Serial.print(F("Connecting to SSID: "));
        Serial.println(currentSSID);

        WiFi.begin(currentSSID, currentPassword);
        while (WiFi.status() != WL_CONNECTED) {
            Serial.print(F("."));
            delay(500);
        }
        Serial.println(F("\nConnected!"));
    }
    return (WiFi.status() == WL_CONNECTED);
}

/**
   Restituisce il segnale delle rete WiFi.
*/
int getWiFiStrength() { return WiFi.RSSI(); }

/**
   Restituisce SSID del WiFi.
*/
String getWiFiSSID() { return WiFi.SSID(); }

/**
  Restituisce true se il nodo � connesso alla rete falso altrimenti.
*/
bool isConnected() { return (WiFi.status() == WL_CONNECTED); }

/**
   Restituisce la password del WiFi.
*/
String getWiFiPassword() { return currentPassword; }

/**
   Restituisce l'udid (indirizzo mac)
*/
void getUDID(char* udid) {
    String mac = WiFi.macAddress();
    mac.toLowerCase();
    mac.replace(":", "");
    mac.toCharArray(udid, 13);
}