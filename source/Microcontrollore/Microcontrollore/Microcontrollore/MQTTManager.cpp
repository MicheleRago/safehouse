/**
   @file MQTTManager.cpp

   @brief Serie di metodi per la gestione di connessioni mqtt
*/
#include "MQTTManager.h"

MQTTClient* mqttClient;    // handles the MQTT communication protocol
WiFiClient networkClient;  // handles the network connection to the MQTT broker

unsigned long lastPublishTime = 0;

void setupMQTTManager(MQTTClientCallbackSimple cb, int mqttBufferSize) {
    mqttClient = new MQTTClient(mqttBufferSize);
    mqttClient->begin(MQTT_BROKERIP, 1883,
        networkClient);  // setup communication with MQTT broker
    mqttClient->onMessage(cb);  // callback on message received from MQTT broker
}

void connectToMQTTBroker() {
    while (!mqttClient->connected()) {
        Serial.print(F("\nConnecting to MQTT broker..."));
        char udid[13];
        getUDID(udid);
        while (!mqttClient->connect(udid, MQTT_USERNAME, MQTT_PASSWORD)) {
            Serial.print(F("."));
            delay(1000);
        }
        if (mqttClient->connected()) {
            Serial.println(F("\nConnected!"));
            mqttSubscribe(MQTT_TOPIC_STATUS);
        }
    }
}

bool mqttClientLoop() { return mqttClient->loop(); }

bool mqttSubscribe(String topicToSubscribe) {
    Serial.println("\nSubscribed to " + topicToSubscribe + " topic!");
    return mqttClient->subscribe(topicToSubscribe);
}

bool mqttPublish(String topic, String txtToPublish) {
    Serial.println("Sending MQTT message: " + topic + " - " + txtToPublish);
    lastPublishTime = millis();
    return mqttClient->publish(topic, txtToPublish);
}
