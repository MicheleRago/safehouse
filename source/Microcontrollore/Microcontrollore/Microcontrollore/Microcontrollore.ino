/*
 Name:		Microcontrollore.ino
 Created:	24/03/2022 14:15:17
 Author:	Michele
*/
#include <ArduinoJson.h>
#include "MemoryManger.h"
#include "NetworkManager.h"
#include "MQTTManager.h"
#include "secrets.h"
#include "PIRSensorManager.h"

bool* sensorsActive;

/**
  Gestisce i messaggi ricevuti tramite mqtt.
  @param topic stringa che contiene il topic su cui si � inviato il messaggio
  @param payload stringa che contiene il messaggio
*/
void mqttMessageReceived(String& topic, String& payload) {
	Serial.println("Incoming MQTT message: " + topic + " - " + payload);
	if (topic.equals(MQTT_TOPIC_STATUS)) {
		writeFile(SENSORS_FILE_PATH, payload);
		readSensorsFile();
	}
}

void readSensorsFile() {
	StaticJsonDocument<512> sensorsFile = getSensorsFile();
	JsonArray active = sensorsFile["active"];

	if (sensorsActive != 0)
		delete[] sensorsActive;

	sensorsActive = new bool[sensorsNumber];
	for (int i = 0; i < sensorsNumber; i++)
		*(sensorsActive + i) = false;
	for (JsonVariant v : active) {
		Serial.println(v.as<int>() - 1);
		sensorsActive[v.as<int>() - 1] = true;
	}
}

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(115200);  // Initialize serial
	connectToWiFi(SECRET_SSID, SECRET_PASS);
	setupMQTTManager(mqttMessageReceived, 512);
	connectToMQTTBroker();
	setupMemoryManager();
	removeFile(SENSORS_FILE_PATH);
	if (!existFile(SENSORS_FILE_PATH))
		firstConfiguration();
	else
		readSensorsFile();
}

/**
  Imposta le operazioni di setup per la FirstBoot mode.
*/
void firstConfiguration() {
	sendHello();
	long time = millis();
	while (true) {
		if (millis() > time + 10000 ) {
			time = millis();
			Serial.println(time);
			if (existFile(SENSORS_FILE_PATH))
				break;
		}
		mqttClientLoop();
	}
}

/**
  Viene utilizzato per persentarsi sulla rete mqtt e ottenere le sue
  configurazioni
*/
void sendHello() {
	StaticJsonDocument<16> doc;
	doc["sensorsNumber"] = sensorsNumber;
	String message;
	serializeJson(doc, message);
	mqttPublish(MQTT_TOPIC_HELLO, message);
}

// the loop function runs over and over again until power down or reset
void loop() {
	delay(10000);
	Serial.println(*(sensorsActive + 0));
	Serial.println(*(sensorsActive + 1));
	Serial.println(*(sensorsActive + 2));
	Serial.println(*(sensorsActive + 3));
}
