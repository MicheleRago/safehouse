/**
   @file MemoryManger.cpp

   @brief Series of methods that help managing the file system.
*/
#include "MemoryManger.h"

/**
  Mounts the file system.

  @return true if the file system is mounted successfully false otherwise
*/
bool setupMemoryManager() {
    if (!LittleFS.begin()) {
        Serial.println("[ERROR] Failed to mount file system");
        return false;
    }
    Serial.println("File system mounted successfully");
    return true;
}

/**
  Get the settings file.

  @return returns the deserialized settings file.
*/
StaticJsonDocument<512> getSensorsFile() {
    StaticJsonDocument<512> settings;

    String settingsFile = readFile(SENSORS_FILE_PATH);
    Serial.println(settingsFile);
    if (settingsFile.isEmpty()) {
        return settings;
    }

    DeserializationError error = deserializeJson(settings, settingsFile);
    if (error) {
        Serial.print(F("[ERROR] deserializeJson() failed: "));
        Serial.println(error.f_str());
        return settings;
    }

    Serial.println("File deserialized successfully");
    return settings;
}

/**
    Get the file at the specified path

    @parm string that contains the file path
    @return the file at the specified path
*/
String readFile(String path) {
    String file;
    File f = LittleFS.open(path, "r");
    if (!f) {
        Serial.println("[ERROR] File could not be opened");
        return file;
    }
    file = f.readString();
    f.close();
    Serial.println("File read successfully");
    return file;
}

/**
  Writes the input string to the file at the specified path.

  @param path string that contains the file path
  @param input string to insert in the settings file.
  @return returns true if the write was successful false otherwise.
*/
bool writeFile(String path, String input) {
    File f = LittleFS.open(path, "w");
    if (!f) {
        Serial.println("[ERROR] File could not be opened");
        return false;
    }
    f.print(input);
    f.close();
    Serial.println("File written successfully");
    return true;
}

/**
  Checks if the file at the specified path exists.

  @param path string that contains the file path
  @return returns true if the file at the specified path exists false otherwise
*/
bool existFile(String path) {
    bool exists = LittleFS.exists(path);
    Serial.println((String)"File exists:" + exists);
    return exists;
}

/**
  Delete  file at the specified path.

  @param path string that contains the file path
  @return returns true if the file at the specified path was successfully
  removed false otherwise.
*/
bool removeFile(String path) {
    if (!LittleFS.remove(path)) {
        Serial.println("[ERROR] File could not be removed");
        return false;
    }
    Serial.println("File removed successfully");
    return true;
}