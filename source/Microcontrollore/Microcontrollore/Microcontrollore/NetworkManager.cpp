/**
   @file MemoryManger.cpp

   @brief Methods that help to manage the network.
*/
#include "NetworkManager.h"

String currentSSID, currentPassword;

/**
   Starts the access point
*/
bool startAP() {
    String ssid = "ESP8266-" + String(ESP.getChipId(), HEX);

    WiFi.mode(WIFI_AP);
    // Note: by default, the access point IP address is 192.168.4.1
    if (!WiFi.softAP(ssid)) {
        Serial.println("[ERROR] Failed to create the Access Point");
        return false;
    }
    Serial.println(
        String("The Access Point(" + ssid + ") was created successfully"));
    return true;
}

/**
   Manages the connection to WiFi
*/
bool connectToWiFi(String ssid, String password) {
    WiFi.disconnect();
    if (WiFi.getAutoConnect()) {
        Serial.println(F("Disabling auto-connect"));
        WiFi.setAutoConnect(false);
    }
    WiFi.persistent(false);
    WiFi.mode(WIFI_STA);
    currentSSID = ssid;
    currentPassword = password;
    return connectToWiFi();
}

/**
   Manages the connection to WiFi
*/
bool connectToWiFi() {
    // connect to WiFi (if not already connected)
    if (WiFi.status() != WL_CONNECTED) {
        Serial.print(F("Connecting to SSID: "));
        Serial.println(currentSSID);

        WiFi.begin(currentSSID, currentPassword);
        while (WiFi.status() != WL_CONNECTED) {
            Serial.print(F("."));
            delay(500);
        }
        Serial.println(F("\nConnected!"));
    }
    return (WiFi.status() == WL_CONNECTED);
}

/**
   Get the WiFi Strength
*/
int getWiFiStrength() { return WiFi.RSSI(); }

/**
   Get the WiFi SSID
*/
String getWiFiSSID() { return WiFi.SSID(); }

/**
  Returns if the node is connected to wifi
*/
bool isConnected() { return (WiFi.status() == WL_CONNECTED); }

/**
   Get the WiFi Password
*/
String getWiFiPassword() { return currentPassword; }

/**
   Get the udid (mac address)
*/
void getUDID(char* udid) {
    String mac = WiFi.macAddress();
    mac.toLowerCase();
    mac.replace(":", "");
    mac.toCharArray(udid, 13);
}