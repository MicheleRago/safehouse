/**
   @file MemoryManger.cpp

   @brief Serie di metodi utilizzati per gestire il file system.
*/
#include "MemoryManger.h"

/**
  Monta il file system.

  @return true se il file system viene montato correttamente falso altrimenti.
*/
bool setupMemoryManager() {
    if (!LittleFS.begin()) {
        Serial.println("[ERROR] Failed to mount file system");
        return false;
    }
    Serial.println("File system mounted successfully");
    return true;
}

/**
  Ottine il file di configurazione dei sensori.

  @return restitusice il file di configurazione dei sensori deserializzato.
*/
StaticJsonDocument<512> getSensorsFile() {
    StaticJsonDocument<512> settings;

    String settingsFile = readFile(SENSORS_FILE_PATH);
    Serial.println(settingsFile);
    if (settingsFile.isEmpty()) {
        return settings;
    }

    DeserializationError error = deserializeJson(settings, settingsFile);
    if (error) {
        Serial.print(F("[ERROR] deserializeJson() failed: "));
        Serial.println(error.f_str());
        return settings;
    }

    Serial.println("File deserialized successfully");
    return settings;
}

/**
    Restituisce il file alla path specificata

    @parm path stringa che contiene il percorso del file
    @return restituisce il file.
*/
String readFile(String path) {
    String file;
    File f = LittleFS.open(path, "r");
    if (!f) {
        Serial.println("[ERROR] File could not be opened");
        return file;
    }
    file = f.readString();
    f.close();
    Serial.println("File read successfully");
    return file;
}

/**
  Scrive la stringa di input nel file nel percorso specificato.

  @param path stringa che contiene il percorso del file
  @param input stringa da inserire nel file.
  @return true se la scrittura � andata a buon fine false altrimenti
*/
bool writeFile(String path, String input) {
    File f = LittleFS.open(path, "w");
    if (!f) {
        Serial.println("[ERROR] File could not be opened");
        return false;
    }
    f.print(input);
    f.close();
    Serial.println("File written successfully");
    return true;
}

/**
  Verifica se il file nel percorso specificato esiste.

  @param path stringa che contiene il percorso del file
  @return true se il file nel percorso specificato esiste false altrimenti.
*/
bool existFile(String path) {
    bool exists = LittleFS.exists(path);
    Serial.println((String)"File exists:" + exists);
    return exists;
}

/**
  Elimina il file nel percorso specificato.

  @param path stringa che contiene il percorso del file
  @return true se il file nel percorso specificato � stato rimosso 
  correttamente false altrimenti.
*/
bool removeFile(String path) {
    if (!LittleFS.remove(path)) {
        Serial.println("[ERROR] File could not be removed");
        return false;
    }
    Serial.println("File removed successfully");
    return true;
}