/**
   @file MQTTManager.cpp

   @brief Serie di metodi per la gestione di connessioni mqtt
*/
#include "MQTTManager.h"

MQTTClient* mqttClient;    // handles the MQTT communication protocol
WiFiClient networkClient;  // handles the network connection to the MQTT broker

unsigned long lastPublishTime = 0;

/**
   Gestisce la configurazione del client MQTT.

   @param cb callback da chimare quando si riceve un messaggio MQTT.
   @param mqttBufferSize grandezza del buffer
*/
void setupMQTTManager(MQTTClientCallbackSimple cb, int mqttBufferSize) {
    mqttClient = new MQTTClient(mqttBufferSize);
    mqttClient->begin(MQTT_BROKERIP, 1883,
        networkClient);  // setup communication with MQTT broker
    mqttClient->onMessage(cb);  // callback on message received from MQTT broker
}

/**
   Gestisce la connessione al broker MQTT.
*/
void connectToMQTTBroker() {
    while (!mqttClient->connected()) {
        Serial.print(F("\nConnecting to MQTT broker..."));
        char udid[13];
        getUDID(udid);
        while (!mqttClient->connect(udid, MQTT_USERNAME, MQTT_PASSWORD)) {
            Serial.print(F("."));
            delay(1000);
        }
        if (mqttClient->connected()) {
            Serial.println(F("\nConnected!"));
            mqttSubscribe(MQTT_TOPIC_STATUS);
        }
    }
}

/**
   Esegue il loop per mantenere la connessione al broker.
*/
bool mqttClientLoop() {
    connectToMQTTBroker();
    return mqttClient->loop(); 
}

/**
   Esegue la sottoscrizione ad un topic.

   @param topicToSubscribe stringa contenente il topic da sottoscrivere
*/
bool mqttSubscribe(String topicToSubscribe) {
    Serial.println("\nSubscribed to " + topicToSubscribe + " topic!");
    return mqttClient->subscribe(topicToSubscribe);
}

/**
   Esegue la pubblicazione di un messaggio.

   @param topic stringa contenente il topic dove pubblicare il messaggio.
   @param txtToPublish stringa contenente il messaggio da pubblicare.
*/
bool mqttPublish(String topic, String txtToPublish) {
    Serial.println("Sending MQTT message: " + topic + " - " + txtToPublish);
    lastPublishTime = millis();
    return mqttClient->publish(topic, txtToPublish);
}
