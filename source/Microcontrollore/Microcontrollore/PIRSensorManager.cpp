/**
   @file PIRSensorManager.cpp

   @brief Serie di metodi per gestire i sensori di movimento
*/
#include "PIRSensorManager.h"

/**
  Imposta i pin connessi ai sensori di movimento in modo corretto
*/
void setupPIRSensorManager() {
	pinMode(pir1, INPUT);
	pinMode(pir2, INPUT);
	pinMode(pir3, INPUT);
	pinMode(pir4, INPUT);
	pinMode(pir5, INPUT);

	long time = millis();
	while (millis() < time + 60000) {
		delay(1);
	}
}

/**
  Ottine lo stato del sensoro di movimento passato come parametro

  @param sensorId id del sensore da controllare
  @return stato del sensoro di movimento passato come parametro
*/
uint8_t getPIRSensorManagerStatus(int sensorId) {
	switch (sensorId) {
	case 1:
		return digitalRead(pir1);
	case 2:
		return digitalRead(pir2);
	case 3:
		return digitalRead(pir3);
	case 4:
		return digitalRead(pir4);
	case 5:
		return digitalRead(pir5);
	default:
		return LOW;
	}
}