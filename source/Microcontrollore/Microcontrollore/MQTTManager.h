/**
   @file MQTTManager.h

   @brief Serie di metodi per la gestione di connessioni mqtt.
*/
#pragma once
#include "NetworkManager.h"
#include <MQTT.h>
#include "secrets.h"

#define MQTT_TOPIC_HELLO "/hello"   
#define MQTT_TOPIC_MOVIMENTO "/movimento"
#define MQTT_TOPIC_STATUS "/status"

void setupMQTTManager(MQTTClientCallbackSimple cb, int mqttBufferSize);
bool mqttClientLoop();
bool mqttSubscribe(String topicToSubscribe);
bool mqttPublish(String topic, String txtToPublish);
void connectToMQTTBroker();