var sensorsSectors = [];    // lista dei sensori ottenuta tramite API
var sectorCount = 0;  // numero di settori in inserimento
var sectors = [], sectorsDel = [], sectorsCh = [], sectorsNew = [];
var codiceProprietario;

$(document).ready(function () {
    AJAXgetSensorsList();
    setInterval(AJAXgetStatus, 2000);
})

function clone(existanceFlag) {
    sectorCount++;
    var dynamicElement = $('.dynamic-element').first();
    var sensorDropdown = $('.dropdown').first();
    var textSectorName = $('.col-md-6.textSectorName').first();
    var btnDel = $('.col-md-1.deleteBtn').first();

    // Modifica degll'elemento dinamico
    var sensorDropdownhtml = '';
    sensorDropdownhtml = '<button class="btn btn-secondary dropdown-toggle" type="button" id="btndr-' + sectorCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    sensorDropdownhtml += 'Sensori</button>';
    sensorDropdownhtml += '<div class="dropdown-menu" aria-labelledby="btndr-' + sectorCount + '">';
    for (let i = 0; i < sensorsSectors.length; ++i) {
        sensorDropdownhtml += '<div class="dropdown-item sensorDropdown"><div class="form-check">';
        sensorDropdownhtml += '<input class="form-check-input" type="checkbox" value="' + sensorsSectors[i].codice + '" id="cs-' + sectorCount + '-' + sensorsSectors[i].codice + '">';
        sensorDropdownhtml += '<label class="form-check-label" for="sensorCheck">' + sensorsSectors[i].codice + '</label>';
        sensorDropdownhtml += '</div></div>';
    }
    sensorDropdownhtml += '</div>';
    sensorDropdown.html(sensorDropdownhtml);
    if (existanceFlag) {
        idstr = 'p-stxt-' + sectorCount;
    } else {
        idstr = 'stxt-' + sectorCount;
    }
    textSectorName.first().html(
        '<input class="form-control" type="text" placeholder="Nome settore" id="' + idstr + '">'
    )
    btnDel.first().html('<p id="' + sectorCount + '" class= "delete" > x</p > ');

    // Clonazione dell'elemento e attach
    dynamicElement.clone().appendTo('.dynamic-stuff').show();
    textSectorName.html('');
    sensorDropdown.html('');
    attach_delete();
}

function createSectorList() {
    for (let i = 0; i < sectors.length; ++i) {
        clone(true);
        sensorNameTxt = $('#p-stxt-' + sectorCount);
        sensorNameTxt.val(sectors[i].nome);
        for (let j = 0; j < sensorsSectors.length; ++j) {
            if (sensorsSectors[j].settore == sectors[i].id) {
                sensCk = $('#cs-' + sectorCount + '-' + sensorsSectors[j].codice);
                sensCk.prop('checked', true);
            }
        }
    }
}

$('.add-one').click(function () {
    clone(false);
});

// Funzionalit� di remove
function attach_delete() {
    $('.delete').off();
    $('.delete').click(function () {
        sid = $(this).prop('id');
        sectorName = $('#p-stxt-' + sid).val();
        if (sectorName != null) {
            for (let i = 0; i < sectors.length; i++) {
                if (sectors[i].nome == sectorName) {
                    sectorsDel.push(sectors[i]);
                    sectors.splice(i, 1);
                }
            }
        }
        $(this).closest('.form-group').remove();
        sectorCount--;
    });
}

$('#btnApplica').click(function () {
    apply();
})

$('#btnGo').click(function () {
    codeInput = $('#code').val();
    if (codeInput == codiceProprietario) {
        $('#errorMessage').text('');
        if (sectorsNew.length != 0) {
            AJAXpostSectors(0, sectorsNew);
        } else {
            AJAXpatchSector(0);
        }
    } else {
        $('#errorMessage').text('Attezione: codice errato!');
        $('#code').val('')
    }
})

function apply() {
    var pFlag = false;  // Se il settore corrente � un settore preesistente
    var sid = '';   // Id estratto dal nome dell'elemento html
    var nIndex = 0, pIndex = 0;    // Indice per i settori nuovi e vecchi
    if (sectorCount == 0) {
        myAlert('Inserisci almeno un settore!');
    } else {
        var sensorsSectorsAppo = structuredClone(sensorsSectors);
        var sectorsNameList = [];
        sectorsNew = [];
        for (let i = 0; i < sensorsSectors.length; ++i) {
            sensorsSectors[i].settore = null;
        }
        for (let i = 1; i <= sectorCount; ++i) {
            pFlag = true;
            try {
                sid = $("input[id^='p-stxt']")[pIndex++].id;
                sectorName = $('#' + sid).val();
                sid = sid[sid.length - 1];
            } catch (error) {
                sid = $("input[id^='stxt']")[nIndex++].id;
                sectorName = $('#' + sid).val();
                sid = sid[sid.length - 1];
                pFlag = false;
            }
            if (sectorName == "") {
                myAlert('Specifica un nome a tutti i settori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo);
                return;
            }
            if (sectorsNameList.indexOf(sectorName) != -1) {
                myAlert('Non � possibile assegnare lo stesso nome a due settori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo); // reset dei valori
                return;
            }

            sectorsNameList.push(sectorName);
            if (pFlag) {
                sectors[i - 1].nome = sectorName;
            } else {
                sectorsNew.push(sectorName);
            }
            var flag = false; // per verificare se � stato aggiunto almeno un sensore al settore
            for (let j = 0; j < sensorsSectors.length; ++j) {
                var sensorCheckbox = $('#cs-' + sid + '-' + sensorsSectors[j]["codice"]);
                if (sensorCheckbox.prop("checked")) {
                    if (sensorsSectors[j].settore == null) {
                        flag = true;
                        if (pFlag)
                            sensorsSectors[j].settore = sectors[i - 1].id;
                        else
                            sensorsSectors[j].settore = sectorName;
                    }
                    else {
                        myAlert('Non � possibile assegnare un sensore a due o pi� settori!');
                        sensorsSectors = structuredClone(sensorsSectorsAppo); // reset dei valori
                        return;
                    }
                }
            }
            if (!flag) {
                myAlert('Non � possibile avere un settore senza sensori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo); // reset dei valori
                return;
            }
            flag = false;
        }
        //console.log(sensorsSectors);
        //console.log(sectors);
        //console.log(sectorsDel);
        //console.log(sectorsNew);
        $('#modalCodice').modal();
    }
}

// Apertura del modal
function myAlert(msg) {
    $('#modalWarningText').text(msg);
    $('#warningModal').modal('show');
}

function AJAXgetSensorsList() {
    var jqxhr = $.get("/api/sensori/", function (data) {
        console.log("AJAX at sensors.json");
        console.log(data);
        sensorsSectors = data;
    }, "json")

        .done(function () {
            AJAXgetSectors();
        })

        .fail(function () {
            console.error("Chiamata AJAXgetSensorList fallita!");
        })
}

function AJAXgetSectors() {
    var jqxhr = $.get("api/settori", function (data) {
        console.log("AJAX at /api/settori");
        console.log(data);
        sectors = data;
        createSectorList();
    }, "json")
}

function AJAXpostSectors(index, sectorsNameList) {
    $.ajax({
        url: '/api/settori/',
        contentType: "application/json",
        type: 'POST',
        data: JSON.stringify({ "nome": sectorsNameList[index], "is_active": false }),
        success: function (response) {
            for (let i = 0; i < sensorsSectors.length; i++) {
                if (sensorsSectors[i].settore == sectorsNameList[index]) {
                    sensorsSectors[i].settore = response.id;
                }
            }
            if (index < sectorsNameList.length - 1)
                AJAXpostSectors(++index, sectorsNameList);
            else
                AJAXpatchSector(0);
        },
    });
}


function AJAXputSensor(index) {
    $.ajax({
        url: '/api/sensori/by-id/' + sensorsSectors[index].id,
        contentType: "application/json",
        type: 'PUT',
        data: JSON.stringify(sensorsSectors[index]),
        success: function (data) {
            if (index < sensorsSectors.length - 1) {
                AJAXputSensor(++index);
            } else {
                // Verifico se sono stati eliminati dei settori attivi
                var flag = false;
                for (let i = 0; i < sectors.length; i++) {
                    if (sectors[i].is_active == true) {
                        flag = true;
                    }
                }
                if (flag) {
                    AJAXpatchInfoSistema(1);
                } else {
                    AJAXpatchInfoSistema(0);
                }
            }
        },
    });
}


function AJAXpatchInfoSistema(status) {
    $.ajax({
        url: '/api/info-sistema',
        contentType: "application/json",
        type: 'PATCH',
        data: '{ "stato_globale":' + status + '}',
        success: function (data) {
            window.location.href = "/settori";
        },
        fail: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}

function AJAXpatchSector(index) {
    $.ajax({
        url: 'api/settori/by-id/' + sectors[index].id,
        contentType: "application/json",
        type: 'PATCH',
        data: '{"nome": "' + sectors[index].nome + '"}',
        success: function (data) {
            if (index < sectors.length - 1) {
                AJAXpatchSector(++index);
            } else {
                if (sectorsDel.length != 0) {
                    AJAXdeleteSector(0);
                } else {
                    AJAXputSensor(0);
                }
            }
        }
    })
}

function AJAXdeleteSector(index) {
    $.ajax({
        url: 'api/settori/by-id/' + sectorsDel[index].id,
        contentType: "application/json",
        type: 'DELETE',
        success: function (data) {
            if (index < sectorsDel.length - 1) {
                AJAXdeleteSector(++index);
            } else {
                AJAXputSensor(0);
            }
        }
    })
}

function AJAXgetStatus() {
    $.get("/api/info-sistema", function (data) {
        codiceProprietario = data.codice_proprietario;
        if (data.stato_globale > 1)
            window.location.replace('/');
    }, "json")
}