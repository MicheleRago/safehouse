var sectorsOn = [], sectorsOff = [], change = [];
var codiceProprietario = '';
var lastStatus = -1;
var attempts = 0;
var timerInterval;
var codiciTemporanei;

$(document).ready(function () {
    doGetStatus();
    AJAXgetSectors();
    setInterval(function () { AJAXgetSectors(); }, 30000); // polling dell'API ogni 30 secondi
    setInterval(function () { doGetStatus(); }, 2000); // polling dell'API ogni 2 secondi
})

function doGetStatus() {
    $.get("/api/info-sistema", function (data) {
        if (codiceProprietario == '')
            codiceProprietario = data.codice_proprietario;
        let status = data.stato_globale;
        if (lastStatus != status) {
            switch (status) {
                case 0:
                    $('#global-status').text('DISATTIVO');
                    $('#modalCodice').modal('hide');
                    break;
                case 1:
                    $('#global-status').text('ATTIVO');
                    $('#modalCodice').modal('hide');
                    break;
                case 2:
                    $('#h5Timer').show();
                    $('#h5Att').show();
                    $('#errorMessage').css("color", "red");
                    $('#global-status').text('MOVIMENTO RILEVATO');
                    $('#modalTitleStatus').text('MOVIMENTO RILEVATO');
                    $('#spanTimer').text('45');
                    attempts = 3;
                    $('#spanAtt').text(attempts);
                    timerInterval = setInterval(manageTimer, 1000);
                    $('#modalCodice').modal({ backdrop: 'static', keyboard: false });
                    break;
                case 3:
                    $('#global-status').text('INTRUSIONE RILEVATA');
                    $('#modalTitleStatus').text('INTRUSIONE RILEVATA');
                    $('#h5Timer').hide();
                    $('#h5Att').hide();
                    $('#errorMessage').css("color", "red");
                    $('#errorMessage').text('');
                    $('#modalCodice').modal({ backdrop: 'static', keyboard: false });
                    clearInterval(timerInterval);
                    break;
                default:
                    break;
            }
            lastStatus = status;
        }

    }, "json")
}

function AJAXgetSectors() {
    var jqxhr = $.get("api/settori", function (data) {
        console.log("AJAX at /api/settori");
        console.log(data);
        splitSectors(data);
    }, "json")
}

/**
 * Esegue lo split della lista di tutti i settori in due liste distinte: lista di settori
 * attivi e lista di settori diattivi
 * 
 * @param {object[]} sectors - lista di tutti i settori
 */
function splitSectors(sectors) {
    sectorsOff = []
    sectorsOn = []
    for (let i = 0; i < sectors.length; ++i) {
        if (sectors[i].is_active == false) {
            sectorsOff.push(sectors[i]);
        } else {
            sectorsOn.push(sectors[i]);
        }
    }
}

$('#btnGo').click(function () {
    codeInput = $('#code').val();
    flagInput = false;
    var errorCode;

    if (codeInput == codiceProprietario) {
        flagInput = true;
        errorCode = 1;
        /*
         Caso di inserimento del codice proprietario, in questo caso verranno disattivati
         tutti i settori
        */ 
        createChanges([]); // lista degli id vuota, vengono disattivati tutti
    } else {
        /*
         Caso in cui si controlla se il codice inserito � un codice temporaneo
        */
        getCodiciTemporanei();

        var trovato = false;
        for (const codiceTemp of codiciTemporanei) {
            console.log(codiceTemp.codice);
            if (codeInput == codiceTemp.codice) {
                trovato = true;

                var date = new Date();
                console.log(new Date(codiceTemp.scadenza).getTime());
                console.log(date.getTime());

                if (new Date(codiceTemp.scadenza).getTime() < date.getTime()) {
                    errorCode = 2;
                } else {
                    
                    settoriIdDaDisattivare = []
                    settoriNomiDaDisattivare = []

                    for (const CodiceSettoreId of codiceTemp.settori) {
                        for (const settore of sectorsOn) {
                            if (settore.id == CodiceSettoreId) {
                                settoriIdDaDisattivare.push(settore.id);
                                settoriNomiDaDisattivare.push(settore.nome);
                                break;
                            }
                        }
                    }
                    if (settoriIdDaDisattivare == 0) {
                        errorCode = 3;
                    }
                    else {
                        flagInput = true;
                        createChanges(settoriIdDaDisattivare);
                    }
                }
                break;
            }
        }

        if (!trovato)
            errorCode = 1;
    }
    if (flagInput) {
        AJAXputSector(0);
        $('#errorMessage').css("color", "green");
        console.log(settoriIdDaDisattivare);
        $('#errorMessage').text('Codice corretto!');
        if (sectorsOn.length > 0) {
            var strSettoriDisattivati = '';
            for (const nome of settoriNomiDaDisattivare)
                strSettoriDisattivati += ' ' + nome;
            $('#modalSensoriDisattivati').modal({ backdrop: 'static', keyboard: false });
            $('#settoriDisattivati').text(strSettoriDisattivati);
        }
        clearInterval(timerInterval);

    } else{
        // Errore inserimento codice sbagliato
        if (lastStatus == 2) {
            --attempts;
            $('#spanAtt').text(attempts);
            if (attempts == 0) {
                // Intrusione rilevata status = 3
                AJAXpatchInfoSistema(3);
            }
        }
        $('#code').val('');

        switch (errorCode) {
            case 1:
                $('#errorMessage').text('Attezione: codice errato!');
                break;
            case 2:
                $('#errorMessage').text('Attezione: codice scaduto!');
                break;
            case 3:
                $('#errorMessage').text('');
                var result = "Attezione: codice che non " + " <br /> " + "disattiva nessun settore attivo!",
                container = document.querySelector("#errorMessage"); 
                listElement = document.createElement("span"),
                listElement.innerHTML = result;
                container.appendChild(listElement);
                break;
        }

    }
})


/**
 * Metodo sincrono per ottenere la lista dei codiciTemporanei
 */
function getCodiciTemporanei() {
    $.ajax({
        url: "/api/codici-temporanei/",
        type: "get",
        async: false,
        success: function (data) {
            codiciTemporanei = data;
        },
        error: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}

/**
 * Crea la lista di settori disattivati selezionando da quelli attivi in base agli id
 * nella lista ids passata come parametro
 * 
 * @param {number[]} ids - lista degli id dei settori che devono essere disattivati
 */
function createChanges(ids) {
    if (ids.length == 0) {  // Lista vuota codice proprietario
        change = structuredClone(sectorsOn);
    } else {
        for (let i = 0; i < sectorsOn.length; ++i) {
            s = structuredClone(sectorsOn[i]);
            if (ids.indexOf(s.id) != -1) {
                change.push(s);
            }
        }
    }
    for (let i = 0; i < change.length; ++i) {
        change[i].is_active = false;
    }
}

function manageTimer() {
    timer = $('#spanTimer').text();
    if (--timer == -1) {
        AJAXpatchInfoSistema(3);
        clearInterval(timerInterval);
    }
    else
        $('#spanTimer').text(timer);
}

function AJAXputSector(index) {
    $.ajax({
        url: '/api/settori/by-id/' + change[index].id,
        contentType: "application/json",
        type: 'PUT',
        async: false,
        data: JSON.stringify(change[index]),
        success: function (data) {
            /*
             Rimuovo i settori che sono stati disattivati grazie al codice inserito
            */
            sectorsOn = sectorsOn.filter(function (obj) {
                return obj.id != change[index].id;
            });
            if (index < change.length - 1) {
                AJAXputSector(++index);
            } else {
                /* 
                 Se dopo le chiamate la lista dei settori attivi ha almeno un elemento
                 allora lo stato globale viene messo a 1 (Attivo)
                */
                if (sectorsOn.length != 0)
                    AJAXpatchInfoSistema(1);
                else
                    AJAXpatchInfoSistema(0)
            }
        },
    });
}

/**
 * Esegue la patch su API /api/info-sistema modificando lo stato globale del sistema nello
 * stato numerico passato per parametro (status)
 * 
 * @param {number} status
 */
function AJAXpatchInfoSistema(status) {
    $.ajax({
        url: '/api/info-sistema',
        contentType: "application/json",
        type: 'PATCH',
        data: '{ "stato_globale":' + status + '}',
        success: function (data) {
            console.log("AJAXPatch InfoSistema - code:" + status);
        },
        fail: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}