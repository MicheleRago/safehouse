﻿var sectorsOn = [], sectorsOff = [], change = [];
var codiceProprietario;

$('document').ready(function () {
    AJAXgetSectors();
    setInterval(AJAXgetStatus, 2000);
})

/**
 * Crea la griglia di card per i settori passati come parametro
 * 
 * @param {object[]} sectors - lista di settori da disegnare
 * @param {string} div - id del blocco div in cui inserire le righe di card
 * @param {string} status - valore nominale assegnato agli id delle righe <row> per distinguere
 *                          quelle appartenenti al gruppo di settori attivi da quelli disattivi
 */
function createGrid(sectors, div, status) {
    var sectorsActiveDiv = $(div);
    let rowcount = Math.floor(sectors.length / 2) + (sectors.length % 2);
    let j = 0;
    for (let i = 0; i < rowcount; ++i) {
        sectorsActiveDiv.append('<div class="row" id="r-' + status + i + '" style="margin-bottom: 15px"></div>')
        while (j < sectors.length && j < (i + 1) * 2) {
            clone(sectors[j], status, "r-" + status + i);
            ++j;
        }
    }
}

/**
 * Esegue il clone e inserisce i dati nel tamplate per la card di un settore
 * 
 * @param {object} sector - oggetto settore da inserire nella riga
 * @param {string} status - valore nominale da inserire nell'oggetto <span> contenente lo stato
 *                          del settore
 * @param {string} row - id dell'oggetto <row> su cui avviene l'append dell'oggetto dinamico
 */
function clone(sector, status, row) {
    var sectorCard = $('#sect-').clone();
    sectorCard.attr("id", "sect-" + sector.id);
    sectorCard.find("span.sec-name").text(sector.nome);
    var spanStatus = sectorCard.find("span.sec-status");
    spanStatus.text(status);
    spanStatus.attr("id", "spst-" + sector.id)
    sectorCard.find("input.form-check-input").attr("id", "sect-check-" + sector.id);
    sectorCard.appendTo('#' + row);
    sectorCard.show();
}

function AJAXgetSectors() {
    var jqxhr = $.get("api/settori", function (data) {
        console.log("AJAX at /api/settori");
        console.log(data);
        splitSectors(data);
        if (sectorsOn.length == 0)
            $('#sAllActiveRow').hide();
        if (sectorsOff.length == 0)
            $('#sAllOffRow').hide();
        createGrid(sectorsOn, '.sectors-on', 'Attivo');
        createGrid(sectorsOff, '.sectors-off', 'Disattivo');
    }, "json")
}

/**
 * Esegue lo split della lista di tutti i settori in due liste distinte: lista di settori
 * attivi e lista di settori diattivi
 * 
 * @param {object[]} sectors - lista di tutti i settori
 */
function splitSectors(sectors) {
    for (let i = 0; i < sectors.length; ++i) {
        if (sectors[i].is_active == false) {
            sectorsOff.push(sectors[i]);
        } else {
            sectorsOn.push(sectors[i]);
        }
    }
}

$('#btnApplica').click(function () {
    change = []; // Annullo eventuali modifiche precedenti
    createChanges(sectorsOn);
    createChanges(sectorsOff);
    if (change.length == 0) {
        $('#warningModal').modal();
    } else {
        $('#modalCodice').modal();
    }
})

function AJAXgetStatus() {
    $.get("/api/info-sistema", function (data) {
        codiceProprietario = data.codice_proprietario;
        if (data.stato_globale > 1)
            window.location.replace('/');
    }, "json")
}

$('#btnGo').click(function () {
    /**
    codeInput = $('#code').val();
    if (codeInput == codiceProprietario) {
        $('#errorMessage').text('');
        console.log(change);
        AJAXputSector(0);
    } else {
        $('#errorMessage').text('Attezione: codice errato!');
        $('#code').val('')
    }**/
    codeInput = $('#code').val();
    flagInput = false;
    var errorCode;

    if (codeInput == codiceProprietario) {
        flagInput = true;
        errorCode = 1;
        /*
         Caso di inserimento del codice proprietario, in questo caso verranno modificati
         i settori
        */
        AJAXputSector(0); 
    } else {
        /*
         Caso in cui si controlla se il codice inserito è un codice temporaneo
        */
        getCodiciTemporanei();

        var trovato = false;
        var errorCode = null;
        for (const codiceTemp of codiciTemporanei) {
            console.log(codiceTemp.codice);
            if (codeInput == codiceTemp.codice) {
                trovato = true;

                var date = new Date();
                console.log(new Date(codiceTemp.scadenza).getTime());
                console.log(date.getTime());

                if (new Date(codiceTemp.scadenza).getTime() < date.getTime()) {
                    errorCode = 2;
                } else {
                    var correttezzaCodice = true;
                    for (const settore of change) {
                        if (!codiceTemp.settori.includes(settore.id)) {
                            correttezzaCodice = false;
                            break;
                        }
                    }
                    if (!correttezzaCodice) {
                        errorCode = 3;
                    }
                    else {
                        flagInput = true;
                        AJAXputSector(0);
                    }
                    break;
                }
            }
        }

        if (!trovato)
            errorCode = 1;
    }
    if(!flagInput) {
        
        $('#code').val('');
        console.log(errorCode);
        switch (errorCode) {
            case 1:
                $('#errorMessage').text('Attezione: codice errato!');
                break;
            case 2:
                $('#errorMessage').text('Attezione: codice scaduto!');
                break;
            case 3:
                $('#errorMessage').text('');
                var result = "Attenzione: il codice inserito non" + "<br />" + "può modificare questi settori",
                    container = document.querySelector("#errorMessage"); 
                listElement = document.createElement("span"),
                    listElement.innerHTML = result;
                container.appendChild(listElement);
                break;
        }

    }
})

/**
 * Metodo sincrono per ottenere la lista dei codiciTemporanei
 */
function getCodiciTemporanei() {
    $.ajax({
        url: "/api/codici-temporanei/",
        type: "get",
        async: false,
        success: function (data) {
            codiciTemporanei = data;
        },
        error: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}

function AJAXputSector(index) {
    $.ajax({
        url: '/api/settori/by-id/' + change[index].id,
        contentType: "application/json",
        type: 'PUT',
        data: JSON.stringify(change[index]),
        success: function (data) {
            /*
             Controllo se il settore modificato e' attivo, se e' attivo lo rimuovo
             dalla lista dei disattivi e lo pusho in attivi, viceversa con i settori
             disattivi nella lista dei cambiamenti (change)
            */
            if (change[index].is_active) {
                sectorsOff = sectorsOff.filter(function (obj) {
                    return obj.id != change[index].id;
                });
                sectorsOn.push(change[index]);
            } else {
                sectorsOn = sectorsOn.filter(function (obj) {
                    return obj.id != change[index].id;
                });
                sectorsOff.push(change[index]);
            }
            if (index < change.length - 1) {
                AJAXputSector(++index);
            } else {
                /* 
                 Se dopo le chiamate la lista dei settori attivi ha almeno un elemento
                 allora viene cambiato lo stato globale del sistema
                */
                if (sectorsOn.length != 0)
                    AJAXpatchInfoSistema(1);
                else
                    AJAXpatchInfoSistema(0)
            }
        },
    });
}

/**
 * Verifica per ogni elemento nella lista di settori se la sua relativa checkbox
 * e' selezionata, nel caso inverte il suo stato e lo aggiunge alla lista dei
 * cambiamenti (change)
 * 
 * @param {object[]} sectors
 */
function createChanges(sectors) {
    for (let i = 0; i < sectors.length; ++i) {
        if ($('#sect-check-' + sectors[i].id).prop('checked')) {
            s = structuredClone(sectors[i]);
            s.is_active = !s.is_active;
            change.push(s);
        }
    }
}

function AJAXpatchInfoSistema(status) {
    $.ajax({
        url: '/api/info-sistema',
        contentType: "application/json",
        type: 'PATCH',
        data: '{ "stato_globale":' + status + '}',
        success: function (data) {
            window.location.href = "/settori";
        },
        fail: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}


$('#sAllActiveCk').change(function () {
    selectAll(this, sectorsOn)
});

$('#sAllOffCk').change(function () {
    selectAll(this, sectorsOff)
});

function selectAll(chk, sectors) {
    if (chk.checked) {
        for (let i = 0; i < sectors.length; ++i) {
            $('#sect-check-' + sectors[i].id).prop('checked', true)
        }
    } else {
        for (let i = 0; i < sectors.length; ++i) {
            $('#sect-check-' + sectors[i].id).prop('checked', false)
        }
    }
}