var publicIp, codiceAppMobile;

function AJAXgetPublicIp(){
    $.get("http://myip.quarzo.top/",function(data){
        publicIp = data;
        createQR();
    })
}

function AJAXgetCodiceAppMobile(){
    $.get("/api/info-sistema",function(data){
        codiceAppMobile = data["codice_app_mobile"];
      createQR();
    }, "json")
}

function createQR(){
    if(publicIp != null && codiceAppMobile != null){
        var QRdata= {address:"http://" + publicIp + ":80",codiceAppMobile:codiceAppMobile};
        var options = {
            text: JSON.stringify(QRdata),
            colorDark: "#ffffff",
			colorLight: "#540E0B",
        };
        
        // Create QRCode Object
        new QRCode(document.getElementById("qrcode"), options);
    }
}

$('document').ready(function(){
    AJAXgetPublicIp();
    AJAXgetCodiceAppMobile();
    setInterval(function () { doGetStatus(); }, 2000); // polling dell'API ogni 2 secondi
})

function doGetStatus() {
    $.get("/api/info-sistema", function (data) {
        let status = data.stato_globale;
        if (status > 1)
            window.location.href = "/";
    }, "json")
}