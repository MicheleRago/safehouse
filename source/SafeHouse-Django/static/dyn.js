var sensors = [];  // lista dei sensori ottenuta tramite API
var sensorsSectors = [];
var sectorCount = 0;  // numero di settori in inserimento


function clone() {
    sectorCount++;
    var dynamicElement = $('.dynamic-element').first();
    var sensorDropdown = $('.dropdown').first();
    var textSectorName = $('.col-md-6.textSectorName').first();

    // Modifica degll'elemento dinamico
    var sensorDropdownhtml = '';
    sensorDropdownhtml = '<button class="btn btn-secondary dropdown-toggle" type="button" id="btndr-' + sectorCount + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    sensorDropdownhtml += 'Settori</button>';
    sensorDropdownhtml += '<div class="dropdown-menu" aria-labelledby="btndr-' + sectorCount + '">';
    for (let i = 0; i < sensorsSectors.length; ++i) {
        sensorDropdownhtml += '<div class="dropdown-item sensorDropdown"><div class="form-check">';
        sensorDropdownhtml += '<input class="form-check-input" type="checkbox" value="' + sensorsSectors[i].codice + '" id="cs-' + sectorCount + '-' + sensorsSectors[i].codice + '">';
        sensorDropdownhtml += '<label class="form-check-label" for="sensorCheck">' + sensorsSectors[i].codice + '</label>';
        sensorDropdownhtml += '</div></div>';
        console.log(sensorsSectors[i]);
    }
    sensorDropdownhtml += '</div>';
    console.log(sensorDropdownhtml);
    sensorDropdown.html(sensorDropdownhtml);
    textSectorName.first().html(
        '<input class="form-control" type="text" placeholder="Nome settore" id="stxt-' + sectorCount + '">'
    )

    // Clonazione dell'elemento e attach
    dynamicElement.clone().appendTo('.dynamic-stuff').show();
    textSectorName.html('');
    sensorDropdown.html('');
    attach_delete();
}

$('.add-one').click(clone);
// Funzionalità di remove
function attach_delete() {
    $('.delete').off();
    $('.delete').click(function () {
        $(this).closest('.form-group').remove();
        sectorCount--;
    });
}

$(document).ready(function () {
    AJAXgetSensorsList();
})

$('#btnGo').click(function () {
    if (sectorCount == 0) {
        myAlert('Inserisci almeno un settore!');
    } else {
        var sensorsSectorsAppo = structuredClone(sensorsSectors);
        var sectorsNameList = [];
        for (let i = 1; i <= sectorCount; ++i) {
            var sectorName = $('#stxt-' + i).val();
            if (sectorName == "") {
                myAlert('Specifica un nome a tutti i settori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo);
                return;
            }
            if (sectorsNameList.indexOf(sectorName) != -1) {
                myAlert('Non è possibile assegnare lo stesso nome a due settori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo); // reset dei valori
                return;
            }
            sectorsNameList.push(sectorName);
            var flag = false; // per verificare se è stato aggiunto almeno un sensore al settore
            for (let j = 0; j < sensorsSectors.length; ++j) {
                var sensorCheckbox = $('#cs-' + i + '-' + sensorsSectors[j]["codice"]);
                if (sensorCheckbox.prop("checked")) {
                    if (sensorsSectors[j].settore == null) {
                        flag = true;
                        sensorsSectors[j].settore = sectorName;
                    }
                    else {
                        myAlert('Non è possibile assegnare un sensore a due o più settori!');
                        sensorsSectors = structuredClone(sensorsSectorsAppo);; // reset dei valori
                        return;
                    }
                }
            }
            if (!flag) {
                myAlert('Non è possibile avere un settore senza sensori!');
                sensorsSectors = structuredClone(sensorsSectorsAppo); // reset dei valori
                return;
            }
            flag = false;
        }
        console.log(sensorsSectors);
        AJAXpostSectors(0, sectorsNameList);
    }
})

// Apertura del modal
function myAlert(msg) {
    $('#modalWarningText').text(msg);
    $('#warningModal').modal('show');
}

function AJAXgetSensorsList() {
    var jqxhr = $.get("/api/sensori/", function (data) {
        console.log("AJAX at sensors.json");
        console.log(data);
        sensorsSectors = data;
    }, "json")

        .done(function () {
            clone();
        })

        .fail(function () {
            console.error("Chiamata AJAXgetSensorList fallita!");
        })
}

function AJAXpostSectors(index, sectorsNameList) {
    //var jqxhr = $.post("/api/settori/", { nome: sectorsNameList[index], is_active: false })

    //    .done(function (response) {
    //        for (let i = 0; i < sensorsSectors.length; i++) {
    //            if (sensorsSectors[i].settore == sectorsNameList[index]) {
    //                sensorsSectors[i].settore = response.id;
    //            }
    //        }
    //        if (index < sectorsNameList.length - 1)
    //            AJAXpostSectors(++index, sectorsNameList);
    //        else
    //            AJAXputSensor(0);
    //    }, "json")

    //    .fail(function () {
    //        console.error("Chiamata AJAXpostSectors fallita!");
    //    })
    $.ajax({
        url: '/api/settori/',
        contentType: "application/json",
        type: 'POST',
        data: JSON.stringify({ "nome": sectorsNameList[index], "is_active": false }),
        success: function (response) {
            for (let i = 0; i < sensorsSectors.length; i++) {
                if (sensorsSectors[i].settore == sectorsNameList[index]) {
                    sensorsSectors[i].settore = response.id;
                }
            }
            if (index < sectorsNameList.length - 1)
                AJAXpostSectors(++index, sectorsNameList);
            else
                AJAXputSensor(0);
        },
    });
}


function AJAXputSensor(index) {
    $.ajax({
        url: '/api/sensori/by-id/' + sensorsSectors[index].id,
        contentType: "application/json",
        type: 'PUT',
        data: JSON.stringify(sensorsSectors[index]),
        success: function (data) {
            if (index < sensorsSectors.length - 1) {
                AJAXputSensor(++index);
            } else {
                AJAXpatchInfoSistema();
            }
        },
    });
}


function AJAXpatchInfoSistema() {
    $.ajax({
        url: '/api/info-sistema',
        contentType: "application/json",
        type: 'PATCH',
        data: '{ "stato_globale": 0}',
        success: function (data) {
            window.location.href = "/";
        },
        fail: function () {
            console.log("AJAXPatch fallita!");
        }
    });
}