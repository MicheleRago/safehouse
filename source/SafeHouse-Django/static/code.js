var codes = [], sectors = [];
var isNew = false;
var nameError = false, sectError = false;
var lastName = '';
var modId = 0;
var codiceProprietario = '';
var codePassed = false;

$('document').ready(function () {
    start();
    AJAXgetSectors();
})

function start() {
    $('#code-list').html('');
    AJAXgetStatus();
    setInterval(AJAXcheckStatus, 2000);
    if (codePassed) {
        AJAXgetCodes();
    }
}

/**
 * Inizializza i valori del modal per la prima volta: crea la lista di settori
 * e inizializza il timepicker
 */
function initModal() {
    // Fill della lista dei settori ricevuto tramite API
    optionsHtml = '';
    for (let i = 0; i < sectors.length; i++) {
        optionsHtml += '<option value="' + sectors[i].id + '">' + sectors[i].nome + '</option>';
    }
    $('#codeSectors').append(optionsHtml);
    // Set data minima timePicker
    $('#codeDateTime').attr("min", new Date().toLocaleDateString('en-ca'));
}

/**
 * Inizlializza i valori del modal per la creazione di un nuovo codice: genera un
 * numero randomico e controlla che sia diverso da tutti i codici gi� presenti nel
 * sistema
 */
function newCode() {
    isNew = true;
    // Generazione codice random
    let flag = true;
    while (flag) {
        flag = false;
        code = Math.floor(100000 + Math.random() * 900000);
        for (let i = 0; i < codes.length; i++) {
            if (code == codes[i].codice)
                flag = true;
        }
    }
    // Titolo modal
    $('#modTitle').text('Creazione nuovo codice - ');
    // Set valori vuoti e numero del codice
    setModalValues('', code, new Date().toLocaleDateString('en-ca'), []);
}

/**
 * Esegue il fill dei valori del modal con i parametri passati in ingresso. Meotodo
 * utilizzato per la modifica / visualizzazione dei dettagli di un codice temporaneo
 * 
 * @param {string} codeName - nome del codice
 * @param {string} codeNumber - numero del codice
 * @param {string} codeDate - data di scadenza del codice
 * @param {number[]} codeSectors - lista di settori coperti dal codice temporaneo
 */
function setModalValues(codeName, codeNumber, codeDate, codeSectors) {
    $('#nameError').hide();
    $('#sectorsError').hide();
    $('#codeNameTxt').val(codeName);
    $('#codeDateTime').attr('value', codeDate);
    $('#codeNmb').text(codeNumber);
    // Clean delle select
    for (let i = 0; i < sectors.length; i++) {
        $('option[value="' + sectors[i].id + '"]').prop('selected', false);
    }
    for (let i = 0; i < codeSectors.length; i++) {
        $('option[value="' + codeSectors[i] + '"]').prop('selected', true);
    }
}

/**
 * Crea la lista di visuale di codici temporanei
*/
function createList() {
    for (i = 0; i < codes.length; i++) {
        c = codes[i];
        // Bisogna controllare se il codice � scaduto
        if (new Date(c.scadenza).getTime() < new Date().getTime())
            clone(c.id, c.nome, c.codice, false);
        else
            clone(c.id, c.nome, c.codice, true);
    }
}

/**
 * Esegue il clone e inserisce i dati nel template per la card di un codice temporaneo
 * 
 * @param {number} id - id del codice temporaneo
 * @param {string} cName - nome del codice
 * @param {string} cNumber - numero del codice
 * @param {boolean} isValid - se il codice � scaduto o meno
 */
function clone(id, cName, cNumber, isValid) {
    var codeCard = $('.box.code-card').first().clone();
    codeCard.attr('id', 'codeCard-' + id);
    codeCard.find('span.code-name').text(cName);
    codeCard.find('span.code-number').text(cNumber);
    var status = codeCard.find('i.fa-solid.fa-circle');
    if (isValid) {
        status.addClass('code-valid');
    } else
        status.addClass('code-expired');
    // Attach Modify
    var modifyBtn = codeCard.find('i.fa-solid.fa-pen');
    modifyBtn.attr('id', 'mdfyBtn-' + id);
    attachModify(modifyBtn);
    // Attach Delete
    var dltBtn = codeCard.find('i.fa-solid.fa-square-xmark');
    dltBtn.attr('id', 'dltBtn-' + id);
    attachDelete(dltBtn);
    codeCard.appendTo('#code-list');
    codeCard.show();
}

/**
 * Esegue l'attach della funzione di modifica all'icona di modifica di una card
 * di un codice temporaneo:
 *      - salva l'id nella variabile globale modId (recuperato per la PUT)
 *      - salva il nome nella variabile globale lastName (recuperato per il controllo
 *      dell'input)
 *      - apre il modal per la modifica dei campi
 *      
 * @param {object} elem - oggetto html a cui attaccare la funzione di modify
 */
function attachModify(elem) {
    elem.click(function (event) {
        isNew = false;
        let id = event.target.id.split('-')[1];
        for (let i = 0; i < codes.length; i++) {
            if (codes[i].id == id) {
                c = codes[i];
                break;
            }
        }
        // Id nella variabile globale per la PUT
        modId = c.id;
        // Nome nella variabile globale
        lastName = c.nome;
        // Titolo modal
        $('#modTitle').text('');
        setModalValues(c.nome, c.codice, c.scadenza.split('T')[0], c.settori);
        $('#code-modal').modal({ backdrop: 'static', keyboard: false });
    })
}

/**
 * Esegue l'attach della funzione di modifica all'icona di eliminazione di una card
 * di un codice temporaneo: 
 *      - salva l'id nella variabile globale modId (recuperato per la DELETE)
 *      - apre la finestra di dialogo per chiedere conferma dell'eliminazione
 *      
 * @param {any} elem - oggetto html a cui attaccare la funzione di delete  
 */
function attachDelete(elem) {
    elem.click(function (event) {
        modId = event.target.id.split('-')[1];
        for (let i = 0; i < codes.length; i++) {
            if (codes[i].id == modId) {
                c = codes[i];
                break;
            }
        }
        $('#answerCodeName').text(c.nome);
        $('#answerModal').modal();
    })
}

/**
 * Bottone di conferma del modal per l'eliminazione di un codice temporaneo
 */
$('#btnYes').click(function () {
    AJAXdeleteCode(modId);
})

/**
 * Raccoglie l'evento click dell'href per l'aggiunta di un nuovo codice temporaneo 
 */
function addBtnClick() {
    $('#code-modal').modal({ backdrop: 'static', keyboard: false });
    newCode();
}

/**
 * Verifica la validit� dell'input della casella di testo contenente il nome del
 * codice temporaneo
 */
function checkCodeName() {
    $('#nameError').hide();
    codeName = $('#codeNameTxt').val();
    if (codeName == '') {
        $('#nameError').text('Inserisci un nome al codice temporaneo!');
        $('#nameError').show();
        nameError = true;
        return;
    } else if (codeName != lastName) {   // In caso di modifica
        flag = false;
        for (let i = 0; i < codes.length; i++) {
            if (codes[i].nome == codeName) {
                flag = true;
                break;
            }
        }
        if (flag) {
            $('#nameError').text("Il nome e' gia' assegnato ad un altro codice!");
            $('#nameError').show();
            nameError = true;
            return;
        }
    }
    nameError = false;
}

$('#codeNameTxt').change(function () {
    checkCodeName();
})

/**
 * @returns {number[]} - lista di settori selezionati nell'input
 */
function getCodeSectors() {
    let codeScts = [];
    for (let i = 0; i < sectors.length; i++) {
        opt = $('option[value="' + sectors[i].id + '"]').is(':selected');
        if (opt)
            codeScts.push(sectors[i].id);
    }
    return codeScts;
}

$('select').change(function () {
    checkSectors();
})

/**
 * Esegue il controllo dell'input sulla selezione di settori (deve essere stato
 * selezionato almeno un settore)
 */
function checkSectors() {
    $('#sectorsError').hide();
    let codeScts = getCodeSectors();
    if (codeScts.length == 0) {
        $('#sectorsError').text('Seleziona almeno un settore da coprire');
        $('#sectorsError').show();
        sectError = true;
    } else {
        sectError = false;
    }
}

/**
 * Esegue il salvataggio delle modifiche effettuate: recupera i dati dagli input
 * e se si tratta di un nuovo settore (isNew==true) esegue la POST del nuovo settore
 * altrimenti esegue la PUT
 */
$('#btnSave').click(function () {
    checkCodeName();
    checkSectors();
    if (!nameError && !sectError) {
        let codeScts = getCodeSectors();
        let codeName = $('#codeNameTxt').val();
        let codeDate = $('#codeDateTime').val();
        let codeNumber = $('#codeNmb').text();
        if (isNew) {
            AJAXpostCode(codeName, codeScts, codeDate, codeNumber);
        } else {
            AJAXputCode(modId, codeName, codeScts, codeDate, codeNumber);
        }
    }
})

$('#btnGo').click(function () {
    codeInput = $('#code').val();
    if (codeInput == codiceProprietario) {
        $('#errorMessage').text('');
        $("#modalCodice").removeClass("in");
        $(".modal-backdrop").remove();
        $("#modalCodice").hide();
        $("#modalCodice").modal('hide');
        codePassed = true;
        start();
    } else {
        $('#errorMessage').text('Attezione: codice errato!');
        $('#code').val('')
    }
})

function AJAXgetSectors() {
    var jqxhr = $.get("api/settori", function (data) {
        console.log("AJAX at /api/settori");
        console.log(data);
        sectors = data;
        initModal();
    }, "json")
}

function AJAXgetCodes() {
    var jqxhr = $.get("api/codici-temporanei", function (data) {
        console.log("AJAX at /api/codici-temporanei");
        console.log(data);
        codes = data;
        createList();
    }, "json")
}

function AJAXgetStatus() {
    $.get("/api/info-sistema", function (data) {
        if (codiceProprietario == '') {
            // Prima chiamata alle API: apertura modal
            codiceProprietario = data.codice_proprietario;
            $('#modalCodice').modal({ backdrop: 'static', keyboard: false });
        }
        if (data.stato_globale > 1)
            window.location.replace('/');
    }, "json")
}

function AJAXpostCode(codeName, codeSectors, codeDate, codeNumber) {
    $.ajax({
        url: '/api/codici-temporanei/',
        contentType: "application/json",
        type: 'POST',
        data: JSON.stringify(
            {
                "nome": codeName,
                "scadenza": codeDate + "T00:00:00Z",
                "codice": codeNumber,
                "settori": codeSectors
            }),
        success: function (response) {
            $('#code-modal').modal('hide');
            start();
        },
        fail: function () {
            console.log("AJAXPost fallita!");
        }
    });
}

function AJAXputCode(id, codeName, codeSectors, codeDate, codeNumber) {
    $.ajax({
        url: '/api/codici-temporanei/by-id/' + id,
        contentType: "application/json",
        type: 'PUT',
        data: JSON.stringify(
            {
                "nome": codeName,
                "scadenza": codeDate + "T00:00:00Z",
                "codice": codeNumber,
                "settori": codeSectors
            }),
        success: function (response) {
            $('#code-modal').modal('hide');
            start();
        },
        fail: function () {
            console.log("AJAXPut fallita!");
        }
    });
}

function AJAXdeleteCode(id) {
    $.ajax({
        url: 'api/codici-temporanei/by-id/' + id,
        contentType: "application/json",
        type: 'DELETE',
        success: function (data) {
            $('#answerModal').modal('hide');
            start();
        },
        fail: function () {
            console.log("AJAXDelete fallita!");
        }
    })
}

function AJAXcheckStatus() {
    $.get("/api/info-sistema", function (data) {
        if (data.stato_globale > 1)
            window.location.replace('/');
    }, "json")
}