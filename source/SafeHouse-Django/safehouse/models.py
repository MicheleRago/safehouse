from django.db import models
from django.core.validators import MinLengthValidator
from solo.models import SingletonModel

class Settore(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=30)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = "Settore"
        verbose_name_plural = "Settori"

class Sensore(models.Model):
    id = models.AutoField(primary_key=True)
    codice = models.CharField(max_length=3, validators=[MinLengthValidator(3)])
    settore = models.ForeignKey(Settore, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.codice

    class Meta:
        verbose_name = "Sensore"
        verbose_name_plural = "Sensori"

class InfoSistema(SingletonModel):
    id = models.AutoField(primary_key=True)
    stato_globale = models.IntegerField(null=True)
    codice_proprietario = models.CharField(max_length=6, validators=[MinLengthValidator(6)])
    codice_app_mobile = models.CharField(max_length=6, validators=[MinLengthValidator(6)])

    class Meta:
        verbose_name = "InfoSistema"
        verbose_name_plural = "InfoSistemi"

class FCMToken(models.Model):
    id = models.CharField(primary_key = True, max_length=200)
    token = models.CharField(max_length=200)

class CodiceTemporaneo(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=200)
    scadenza = models.DateTimeField()
    settori = models.ManyToManyField(Settore)
    codice = models.CharField(max_length=6, validators=[MinLengthValidator(6)])

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = "Codice Temporaneo"
        verbose_name_plural = "Codici Temporanei"