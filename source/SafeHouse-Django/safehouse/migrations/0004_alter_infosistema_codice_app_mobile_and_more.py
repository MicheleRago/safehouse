# Generated by Django 4.0.3 on 2022-03-30 20:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('safehouse', '0003_settore_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infosistema',
            name='codice_app_mobile',
            field=models.CharField(max_length=6, validators=[django.core.validators.MinLengthValidator(6)]),
        ),
        migrations.AlterField(
            model_name='infosistema',
            name='codice_proprietario',
            field=models.CharField(max_length=6, validators=[django.core.validators.MinLengthValidator(6)]),
        ),
        migrations.AlterField(
            model_name='settore',
            name='nome',
            field=models.CharField(max_length=3, validators=[django.core.validators.MinLengthValidator(3)]),
        ),
    ]
