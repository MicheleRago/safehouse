from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from .models import CodiceTemporaneo, Sensore, InfoSistema, Settore, CodiceTemporaneo

from datetime import datetime, timedelta
import time

# Create your tests here.
class SuiteTest(StaticLiveServerTestCase):
    def test_config(self):
        """
        Test per la pagina di configurazione: verifica i casi limite
        di configurazione del sistema specificati dai requisiti (test di accettazione)
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = None
        info_sistema.save()
        Sensore.objects.all().delete()
        sensori = ["001", "002","003","004"]
        for s in sensori:
            Sensore.objects.create(codice=s)
        time.sleep(3)

        """ decommentare per rendere il test headless
        options = Options()
        options.headless = True
        driver = webdriver.Chrome(chrome_options=options)
        """

        driver = webdriver.Chrome()
        driver.get(self.live_server_url)
        time.sleep(2)
        button = driver.find_element_by_xpath("//a[contains(text(),'Prosegui')]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[@id='warningModal']/div[1]/div[1]/div[1]/button[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[2]/main[1]/div[2]/form[1]/fieldset[1]/div[1]/div[1]/div[1]/div[4]/p[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[@id='warningModal']/div[1]/div[1]/div[1]/button[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//p[contains(text(),'+ Aggiungi nuovo settore')]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        txt = driver.find_element_by_id('stxt-1')
        txt.send_keys('Sala')
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[@id='warningModal']/div[1]/div[1]/div[1]/button[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btndr-1']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-1-001']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-1-002']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//p[contains(text(),'+ Aggiungi nuovo settore')]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        txt = driver.find_element_by_id('stxt-2')
        txt.send_keys('Cucina')
        button = driver.find_element_by_xpath("//button[@id='btndr-2']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-001']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-002']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[@id='warningModal']/div[1]/div[1]/div[1]/button[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        txt = driver.find_element_by_id('stxt-2')
        txt.clear()
        txt.send_keys('Sala')
        button = driver.find_element_by_xpath("//button[@id='btndr-2']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-001']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-002']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-003']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//input[@id='cs-2-004']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(2)
        button = driver.find_element_by_xpath("//body/div[@id='warningModal']/div[1]/div[1]/div[1]/button[1]")
        ActionChains(driver).move_to_element(button).click(button).perform()
        txt = driver.find_element_by_id('stxt-2')
        txt.clear()
        txt.send_keys('Cucina')
        time.sleep(2)
        button = driver.find_element_by_xpath("//button[@id='btnGo']")
        ActionChains(driver).move_to_element(button).click(button).perform()
        time.sleep(5)


    def test_index(self):
        """
        Test per la pagina di visualizzazione dello stato del sistema: verifica che al cambiare
        dello stato globale del sistema di allarme venga visualizzato nella pagina il corrispondente
        valore nominale
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        info_sistema.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url)
        time.sleep(2)
        txt_status = driver.find_elements_by_xpath("//span[@id='global-status']")
        assert txt_status[0].text == "DISATTIVO"
        info_sistema.stato_globale = 1
        info_sistema.save()
        time.sleep(4)
        txt_status = driver.find_elements_by_xpath("//span[@id='global-status']")
        assert txt_status[0].text == "ATTIVO"
        info_sistema.stato_globale = 2
        info_sistema.save()
        time.sleep(4)
        txt_status = driver.find_elements_by_xpath("//span[@id='global-status']")
        assert txt_status[0].text == "MOVIMENTO RILEVATO"
        info_sistema.stato_globale = 3
        info_sistema.save()
        time.sleep(4)
        txt_status = driver.find_elements_by_xpath("//span[@id='global-status']")
        assert txt_status[0].text == "INTRUSIONE RILEVATA"
    
    def test_movimento_rilevato(self):
        """
        Test per la pagina di visualizzazione dello stato del sistema in movimento rilevato: verifica 
        che al cambiare dello stato globale in movmento rilevato appaia il pannelo per inserire il codice
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        info_sistema.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url)
        time.sleep(2)
        info_sistema.stato_globale = 2
        info_sistema.save()
        time.sleep(2)
        txt_status = driver.find_elements_by_xpath('//*[@id="modalCodice"]/div/div')

    def test_settori(self):
        """
        Test per la visualizzazione dello stato dei settori: verifica che i settori attivi si trovino
        nella posizione corretta, div associato, e che il corrispettivo div per i settori
        disattivi sia vuoto, e viceversa per una seconda verifica
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        info_sistema.save()
        Settore.objects.all().delete()
        sect = Settore(nome='S1', is_active=False)
        sect.save()
        sect = Settore(nome='S2', is_active=False)
        sect.save()
        sect = Settore(nome='S3', is_active=False)
        sect.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        divActive = driver.find_elements_by_class_name("sectors-on")
        assert divActive[0].text == ''
        sectorsList = Settore.objects.all()
        for s in sectorsList:
            sectStatus = driver.find_element_by_id("spst-" + str(s.id))
            assert sectStatus.text == 'Disattivo'
        Settore.objects.all().delete()
        sect = Settore(nome='S1', is_active=True)
        sect.save()
        sect = Settore(nome='S2', is_active=True)
        sect.save()
        sect = Settore(nome='S3', is_active=True)
        sect.save()
        time.sleep(2)
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        divOff = driver.find_elements_by_class_name("sectors-off")
        assert divOff[0].text == ''
        sectorsList = Settore.objects.all()
        for s in sectorsList:
            sectStatus = driver.find_element_by_id("spst-" + str(s.id))
            assert sectStatus.text == 'Attivo'
        

    def test_modifica_settori_codice_proprietario(self):
        """
        Test per la modifica dello stato dei settori: modifica lo stato di alcuni settori
        verificando il corretto inserimento del codice proprietario e messaggi di errore.
        Una volta modificato lo stato dei settori verifica se l'home page ha aggionrato
        lo stato globale del sistema di allarme
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        codice_prop = info_sistema.codice_proprietario
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=True)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        # Applica modifiche senza selezionare alcun settore
        btn_applica = driver.find_element_by_id('btnApplica')
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(1)
        btn_cls_modal = driver.find_element_by_id('btnModalCls')
        ActionChains(driver).move_to_element(btn_cls_modal).click(btn_cls_modal).perform()
        # Attivazione settore S2 e disattivazione settore S1
        checkBox = driver.find_element_by_id('sect-check-' + str(s1.id))
        ActionChains(driver).move_to_element(checkBox).click(checkBox).perform()
        checkBox = driver.find_element_by_id('sect-check-' + str(s2.id))
        ActionChains(driver).move_to_element(checkBox).click(checkBox).perform()
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(2)
        txt_code = driver.find_element_by_id('code')
        btn_go = driver.find_element_by_id('btnGo')
        span_error = driver.find_element_by_id('errorMessage')
        # Inserimento di un codice errato
        txt_code.send_keys('123456')
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert span_error.text == 'Attezione: codice errato!'
        # Inserimento codice corretto e invio
        txt_code.send_keys(codice_prop)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(4)
        # Verifica dello stato dei settori dopo le modifiche
        assert driver.find_element_by_id('spst-' + str(s1.id)).text == 'Disattivo'
        assert driver.find_element_by_id('spst-' + str(s2.id)).text == 'Attivo'
        # Verifica dello stato globale del sistema nella pagina principale
        driver.get(self.live_server_url + '/')
        time.sleep(2)
        assert driver.find_element_by_id('global-status').text == 'ATTIVO'
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        # Disattivo tutti i settori
        btn_applica = driver.find_element_by_id('btnApplica')
        checkBox = driver.find_element_by_id('sect-check-' + str(s2.id))
        ActionChains(driver).move_to_element(checkBox).click(checkBox).perform()
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(2)
        txt_code = driver.find_element_by_id('code')
        btn_go = driver.find_element_by_id('btnGo')
        txt_code.send_keys(codice_prop)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(4)
        assert driver.find_element_by_id('spst-' + str(s2.id)).text == 'Disattivo'
        # Verifica dello stato globale del sistema nella pagina principale
        driver.get(self.live_server_url + '/')
        time.sleep(2)
        assert driver.find_element_by_id('global-status').text == 'DISATTIVO'

    def test_modifica_settori_codice_temporaneo(self):
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        codice_prop = info_sistema.codice_proprietario
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=True)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        txt_code = driver.find_element_by_id('code')
        spn_error_message = driver.find_element_by_id('errorMessage')
        btn_go = driver.find_element_by_id('btnGo')
        # Applica modifiche senza selezionare alcun settore
        btn_applica = driver.find_element_by_id('btnApplica')
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(1)
        btn_cls_modal = driver.find_element_by_id('btnModalCls')
        ActionChains(driver).move_to_element(btn_cls_modal).click(btn_cls_modal).perform()
        # Inserimento di un codice sbagliato
        checkBox = driver.find_element_by_id('sect-check-' + str(s1.id))
        ActionChains(driver).move_to_element(checkBox).click(checkBox).perform()
        checkBox = driver.find_element_by_id('sect-check-' + str(s2.id))
        ActionChains(driver).move_to_element(checkBox).click(checkBox).perform()
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(2)
        txt_code.send_keys('123456')
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice errato!'
        # Inserimento di un codice scaduto
        c1 = CodiceTemporaneo(nome='C1', scadenza=datetime.now()- timedelta(days=1), codice="000002")
        c1.save()
        c1.settori.set([s1,s2])
        txt_code.send_keys(c1.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice scaduto!'
        # Inserimento di un codice che non modifica i settori selezionati
        c2 = CodiceTemporaneo(nome='C2', scadenza=datetime.now(), codice="000003")
        c2.save()
        c2.settori.set([s2])
        txt_code.send_keys(c2.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attenzione: il codice inserito non\npu\u00f2 modificare questi settori'
        # Inserimento di un codice che modifica i settori selezionati
        c3 = CodiceTemporaneo(nome='C3', scadenza=datetime.now(), codice="000004")
        c3.save()
        c3.settori.set([s1,s2])
        txt_code.send_keys(c3.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(2)
        assert Settore.objects.get(id=s1.id).is_active==False

    def test_mobile_app (self):
        """
        Test per verificare il corretto funzionamento della schermata AppMobile in particolare
        nella corretta generazione del codice QR.
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        info_sistema.codice_app_mobile = "226398"
        info_sistema.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + "/appmobile")
        time.sleep(5)
        divQR = driver.find_elements_by_id("qrcode")
        assert divQR[0].size['height'] != 0
        

    def test_disattivazione_allarme_codice_proprietario(self):
        """
        Test per verificare il corretto funzionamento e il cambio dello stato globale durante
        la disattivazione dell'allarme con il codice proprietario; vengono testati, pltre che il
        corretto inserimento del codice proprietario, i casi limite quali:
            - inserimento di un codice sbagliato per 3 volte consecutive
            - attesa della scadenza del timer di 45 secondi
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 1
        info_sistema.save()
        codice_prop = info_sistema.codice_proprietario
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=True)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/')
        time.sleep(2)
        # Moviemnto rilevato
        info_sistema.stato_globale = 2
        info_sistema.save()
        time.sleep(3)
        # Inserimento codice errato per tre volte di fila
        txt_code = driver.find_element_by_id('code')
        spn_attempts = driver.find_element_by_id('spanAtt')
        spn_error_message = driver.find_element_by_id('errorMessage')
        btn_go = driver.find_element_by_id('btnGo')
        for i in range(3,0,-1):
            assert spn_attempts.text == str(i)
            txt_code.send_keys('123456')
            ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
            time.sleep(1)
            assert spn_error_message.text == 'Attezione: codice errato!'
            time.sleep(2)
        time.sleep(4)
        # Intrusione rilevata
        info_sistema = InfoSistema.get_solo()
        assert info_sistema.stato_globale == 3
        h_modal_status = driver.find_element_by_id('modalTitleStatus')
        assert h_modal_status.text == 'INTRUSIONE RILEVATA'
        # Inserimento di un codice errato
        txt_code.send_keys('123456')
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice errato!'
        time.sleep(2)
        # Inserimento del codice proprietario e disattivazione allarme
        txt_code.send_keys(codice_prop)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(3)
        # Verifica cambiamento di stato allarme
        info_sistema = InfoSistema.get_solo()
        assert info_sistema.stato_globale == 0
        spn_global_status = driver.find_element_by_id('global-status')
        assert spn_global_status.text == 'DISATTIVO'
        # Verifica che tutti i settori siano disattivati
        sectors = Settore.objects.all()
        assert sectors[0].is_active == False
        assert sectors[1].is_active == False
        assert sectors[2].is_active == False
        # Attivazione di un settore (o viene generato un errore dalle API)
        sectors[0].is_active = True
        sectors[0].save()
        # Modifca stato globale in movimento rilevato
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 2
        info_sistema.save()
        time.sleep(3)
        # Attesa della scadenza del timer di 45 secondi
        time.sleep(46)
        # Intrusione rilevata
        info_sistema = InfoSistema.get_solo()
        assert info_sistema.stato_globale == 3
        h_modal_status = driver.find_element_by_id('modalTitleStatus')
        assert h_modal_status.text == 'INTRUSIONE RILEVATA'
        time.sleep(2)

    def test_disattivazione_allarme_codice_proprietario(self):
        """
        Test per verificare il corretto funzionamento e il cambio dello stato globale durante
        la disattivazione dell'allarme con il codice temporaneo; vengono testati,i seguenti 
        casi:
            - inserimento di un codice sbagliato
            - inserimento di un codice scaduto
            - inserimento di un codice che non disattiva nessun settore attivo
            - inserimento di un codice che disattiva alcuni settori attivi
            - inserimento di un codice che disattiva tutti i settori attivi
        """
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 3
        info_sistema.codice_proprietario = "000001"
        info_sistema.save()
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=True)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        s4 = Settore(nome='S4', is_active=True)
        s4.save()
        CodiceTemporaneo.objects.all().delete()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/')
        time.sleep(2)
        
        txt_code = driver.find_element_by_id('code')
        spn_error_message = driver.find_element_by_id('errorMessage')
        btn_go = driver.find_element_by_id('btnGo')

        # Inserimento di un codice sbagliato
        txt_code.send_keys('123456')
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice errato!'
        # Inserimento di un codice scaduto
        c1 = CodiceTemporaneo(nome='C1', scadenza=datetime.now()- timedelta(days=1), codice="000002")
        c1.save()
        c1.settori.set([s1])
        txt_code.send_keys(c1.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice scaduto!'
        # Inserimento di un codice che non disattiva nessun settore attivo
        c2 = CodiceTemporaneo(nome='C2', scadenza=datetime.now(), codice="000003")
        c2.save()
        c2.settori.set([s2])
        txt_code.send_keys(c2.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Attezione: codice che non\ndisattiva nessun settore attivo!'
        # Inserimento di un codice che non disattiva nessun settore attivo
        c3 = CodiceTemporaneo(nome='C3', scadenza=datetime.now(), codice="000004")
        c3.save()
        c3.settori.set([s1])
        txt_code.send_keys(c3.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        txt_message_settori_disattivati = driver.find_element_by_xpath('/html/body/div[3]/div/div/div[2]/h1')
        assert txt_message_settori_disattivati.text == 'I seguenti settori sono stati disattivati: ' + s1.nome
        # Inserimento di un codice che disattiva tutti i settori attivi
        s1.is_active = True
        s1.save()
        c4 = CodiceTemporaneo(nome='C4', scadenza=datetime.now(), codice="000005")
        c4.save()
        c4.settori.set([s1,s4])
        info_sistema.stato_globale = 3
        info_sistema.save()
        driver.refresh()
        time.sleep(2)
        txt_code = driver.find_element_by_id('code')
        spn_error_message = driver.find_element_by_id('errorMessage')
        btn_go = driver.find_element_by_id('btnGo')
        txt_code.send_keys(c4.codice)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(1)
        assert spn_error_message.text == 'Codice corretto!'

    def test_selezione_tutti_settori(self):
        info_sistema = InfoSistema.get_solo()
        info_sistema.stato_globale = 0
        info_sistema.save()
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=False)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        # Verifico che non sia visibile la checkbox dei settori attivi
        s_all_ck = driver.find_element_by_id('sAllActiveCk')
        assert s_all_ck.is_displayed() == False
        # Attivazione di tutti i settori con un click della checkbox
        s_all_ck = driver.find_element_by_id('sAllOffCk')
        ActionChains(driver).move_to_element(s_all_ck).click(s_all_ck).perform()
        time.sleep(1)
        # Verifico che le checkbox siano selezionate
        sectors = Settore.objects.all();
        for s in sectors:
            ck = driver.find_element_by_id('sect-check-' + str(s.id))
            assert ck.get_attribute('checked') == "true"
        sectors = Settore.objects.all()
        for s in sectors:
            s.is_active = True
            s.save()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        # Verifico che non sia visibile la checkbox dei settori attivi
        s_all_ck = driver.find_element_by_id('sAllOffCk')
        assert s_all_ck.is_displayed() == False
        # Disattivazione di tutti i settori con un click della checkbox
        s_all_ck = driver.find_element_by_id('sAllActiveCk')
        ActionChains(driver).move_to_element(s_all_ck).click(s_all_ck).perform()

    def test_modifica_composizione_settori(self):
        """
        Test per verificare il corretto funzionamento della modifica dei settori.
        """
        info_sistema = InfoSistema.get_solo()
        codice_proprietario = info_sistema.codice_proprietario
        info_sistema.stato_globale = 0
        info_sistema.save()
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=False)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        Sensore.objects.all().delete()
        sens1 = Sensore(codice='001', settore=s1)
        sens1.save()
        sens2 = Sensore(codice='002', settore=s1)
        sens2.save()
        sens3 = Sensore(codice='003', settore=s2)
        sens3.save()
        sens4 = Sensore(codice='004', settore=s3)
        sens4.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/settori')
        time.sleep(2)
        mod_sectors_link = driver.find_element_by_id('mod-sectors')
        ActionChains(driver).move_to_element(mod_sectors_link).click(mod_sectors_link).perform()
        time.sleep(3)
        # Modifico il primo settore
        s1_text = driver.find_element_by_id('p-stxt-1')
        s1_text.send_keys('-modificato')
        s1_drpd = driver.find_element_by_id('btndr-1')
        ActionChains(driver).move_to_element(s1_drpd).click(s1_drpd).perform()
        time.sleep(1)
        chk = driver.find_element_by_id('cs-1-002')
        ActionChains(driver).move_to_element(chk).click(chk).perform()
        ActionChains(driver).move_to_element(s1_drpd).click(s1_drpd).perform()
        # Aggiungo un nuovo settore
        add = driver.find_element_by_id('btnAdd')
        ActionChains(driver).move_to_element(add).click(add).perform()
        new_text = driver.find_element_by_id('stxt-4')
        new_text.send_keys('Nuovo settore')
        new_drpd = driver.find_element_by_id('btndr-4')
        ActionChains(driver).move_to_element(new_drpd).click(new_drpd).perform()
        time.sleep(1)
        chk = driver.find_element_by_id('cs-4-002')
        ActionChains(driver).move_to_element(chk).click(chk).perform()
        ActionChains(driver).move_to_element(new_drpd).click(new_drpd).perform()
        time.sleep(1)
        # Applico le modifiche
        btn_applica = driver.find_element_by_id('btnApplica')
        ActionChains(driver).move_to_element(btn_applica).click(btn_applica).perform()
        time.sleep(1)
        txt_code = driver.find_element_by_id('code')
        txt_code.send_keys(codice_proprietario)
        btn_go = driver.find_element_by_id('btnGo')
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        

    def test_creazione_codice_temporaneo(self):
        """
        Test per la verifica del corretto funzionamento della creazione di un nuovo codice
        temporaneo
        """
        info_sistema = InfoSistema.get_solo()
        codice_prop = info_sistema.codice_proprietario
        info_sistema.stato_globale = 0
        info_sistema.save()
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=False)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        CodiceTemporaneo.objects.all().delete()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/codici')
        time.sleep(2)
        # Inserimento codice proprietario
        txt_code = driver.find_element_by_id('code')
        btn_go = driver.find_element_by_id('btnGo')
        txt_code.send_keys(codice_prop)
        time.sleep(1)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        # Aggiungo un nuovo codice
        btn_add = driver.find_element_by_id('btnAdd')
        ActionChains(driver).move_to_element(btn_add).click(btn_add).perform()
        time.sleep(2)
        # Nome del codice
        code_name_txt = driver.find_element_by_id('codeNameTxt')
        code_name_txt.send_keys('Nuovo codice')
        # Salvataggio del numero per verifica
        code_number = driver.find_element_by_id('codeNmb').text
        # Data di scadenza
        date_picker = driver.find_element_by_id('codeDateTime')
        date_picker.send_keys("01/01/2222")
        sectors = Select(driver.find_element_by_id('codeSectors'))
        sectors.select_by_index(0)
        sectors.select_by_index(1)
        # Salvataggio
        btn_save = driver.find_element_by_id('btnSave')
        ActionChains(driver).move_to_element(btn_save).click(btn_save).perform()
        time.sleep(3)
        # Verifico l'avvenuto salvataggio del codice lato server
        codici = CodiceTemporaneo.objects.all()
        settori = Settore.objects.all()
        assert len(codici) == 1
        assert codici[0].codice == code_number
        assert codici[0].nome == 'Nuovo codice'
        assert codici[0].settori.all()[0].id == settori[0].id
        assert codici[0].settori.all()[1].id == settori[1].id

    def test_gestione_codice_temporanei(self):
        """
        Test per la verifica del corretto funzionamento della gestione dei codici temporanei:
            - modifica un codice temporaneo
            - verifica la visualizzazione delle modifiche
            - elimina un codice temporaneo
            - verifca le modifiche server side
        """
        info_sistema = InfoSistema.get_solo()
        codice_prop = info_sistema.codice_proprietario
        info_sistema.stato_globale = 0
        info_sistema.save()
        Settore.objects.all().delete()
        s1 = Settore(nome='S1', is_active=False)
        s1.save()
        s2 = Settore(nome='S2', is_active=False)
        s2.save()
        s3 = Settore(nome='S3', is_active=False)
        s3.save()
        CodiceTemporaneo.objects.all().delete()
        sectors = Settore.objects.all()
        cod1 = CodiceTemporaneo(nome='Primo codice', scadenza='1999-05-25T00:00:00Z',
                                    codice = 123456)
        cod1.save()
        cod1 = CodiceTemporaneo.objects.all()[0]
        cod1.settori.add(sectors[0])
        cod1.settori.add(sectors[1])
        cod1.save()
        driver = webdriver.Chrome()
        driver.get(self.live_server_url + '/codici')
        time.sleep(2)
        # Inserimento codice proprietario
        txt_code = driver.find_element_by_id('code')
        btn_go = driver.find_element_by_id('btnGo')
        txt_code.send_keys(codice_prop)
        time.sleep(1)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(2)
        # Visualizzazione del codice
        code_name = driver.find_elements_by_class_name('code-name')[1].text
        assert code_name == 'Primo codice'
        code_status = driver.find_elements_by_class_name('code-expired')
        assert len(code_status) == 1
        code_number = driver.find_elements_by_class_name('code-number')[1].text
        assert code_number == '123456'
        # Modifica del codice
        btn_modify = driver.find_elements_by_class_name('fa-pen')[1]
        ActionChains(driver).move_to_element(btn_modify).click(btn_modify).perform()
        time.sleep(2)
        code_name_txt = driver.find_element_by_id('codeNameTxt')
        code_name_txt.send_keys(' - modificato')
        date_picker = driver.find_element_by_id('codeDateTime')
        date_picker.send_keys("01/02/2222")
        sectors = Select(driver.find_element_by_id('codeSectors'))
        sectors.deselect_all()
        sectors.select_by_index(0)
        # Salvataggio
        btn_save = driver.find_element_by_id('btnSave')
        ActionChains(driver).move_to_element(btn_save).click(btn_save).perform()
        driver.get(self.live_server_url + '/codici')
        time.sleep(3)
        # Inserimento codice proprietario
        txt_code = driver.find_element_by_id('code')
        btn_go = driver.find_element_by_id('btnGo')
        txt_code.send_keys(codice_prop)
        time.sleep(1)
        ActionChains(driver).move_to_element(btn_go).click(btn_go).perform()
        time.sleep(2)
        # Visualizzazione delle modifiche
        code_name = driver.find_elements_by_class_name('code-name')[1].text
        assert code_name == 'Primo codice - modificato'
        code_status = driver.find_elements_by_class_name('code-valid')
        assert len(code_status) == 1
        # Eliminazione del codice
        btn_delete = driver.find_elements_by_class_name('fa-square-xmark')[1]
        ActionChains(driver).move_to_element(btn_delete).click(btn_delete).perform()
        time.sleep(3)
        btn_yes = driver.find_element_by_id('btnYes')
        ActionChains(driver).move_to_element(btn_yes).click(btn_yes).perform()
        time.sleep(3)
        # Verifica delle modifiche server side
        codici = CodiceTemporaneo.objects.all()
        assert len(codici) == 0