from functools import partial
from rest_framework import generics
from rest_framework.serializers import Serializer
from .serializers import *
from safehouse.models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from safehouse.updater_thread import *
from safehouse.fcm import send_intrusion_notification
from rest_framework import status

class InfoSistemaView(APIView):
    """
    API che gestisce le informazioni del sistema
    """

    def get(self, request):
        info_sistema = InfoSistema.objects.get()
        serializer = InfoSistemaSerializer(info_sistema)
        return Response(serializer.data)

    def put(self, request):
        info_sistema = InfoSistema.objects.get()
        serializer = InfoSistemaSerializer(info_sistema, data=request.data)
        if serializer.is_valid():
            serializer.save()
            if serializer.data['stato_globale']==3:
                send_intrusion_notification()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request):
        info_sistema = InfoSistema.objects.get()
        serializer = InfoSistemaSerializer(info_sistema, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            if serializer.data['stato_globale']==3:
                send_intrusion_notification()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class SettoreListCreateAPIView(generics.ListCreateAPIView):
    """
    API che restituisce tutti i settori
    """
    queryset = Settore.objects.all().order_by("pk")
    serializer_class = SettoreSerializer


class SettoreRetrieveUpdateDestroyAPIView(APIView):
    """
    API per gestire uno specifico settore 
    """

    def get(self, request, pk):
        settore = Settore.objects.get(id=pk)
        serializer = SettoreSerializer(settore)
        return Response(serializer.data)

    def put(self, request, pk):
        settore = Settore.objects.get(id=pk)
        serializer = SettoreSerializer(settore, data=request.data)
        if serializer.is_valid():
            serializer.save()
            tList = get_tList()
            if len(tList) > 0:
                print("Updater Thread: one thread is already alive. Killed")
                tList[len(tList) - 1].stop()
            t = StoppableThread(target=update_sectors)
            tList.append(t)
            tList[len(tList) - 1].start()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk):
        settore = Settore.objects.get(id=pk)
        serializer = SettoreSerializer(settore, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        settore = Settore.objects.get(id=pk)
        settore.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class SensoreListCreateAPIView(generics.ListCreateAPIView):
    """
    API che restituisce tutti i sensori
    """
    queryset = Sensore.objects.all().order_by("pk")
    serializer_class = SensoreSerializer


class SensoreRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API per gestire uno specifico sensore 
    """
    queryset = Sensore.objects.all()
    serializer_class = SensoreSerializer


class FCMTokenListCreateAPIView(generics.ListCreateAPIView):
    """
    API che restituisce tutti gli FCM Token
    """
    queryset = FCMToken.objects.all().order_by("pk");
    serializer_class = FCMTokenSerializer 


class FCMTokenRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API per gestire uno specifico FCM Token
    """
    queryset = FCMToken.objects.all()
    serializer_class = FCMTokenSerializer


class CodiceTemporaneoListCreateAPIView(generics.ListCreateAPIView):
    """
    API che restituisce tutti i Codici temporanei
    """
    queryset = CodiceTemporaneo.objects.all().order_by("pk");
    serializer_class = CodiceTemporaneoSerializer 


class CodiceTemporaneoRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API per gestire uno specifico Codice temporaneo
    """
    queryset = CodiceTemporaneo.objects.all()
    serializer_class = CodiceTemporaneoSerializer 

