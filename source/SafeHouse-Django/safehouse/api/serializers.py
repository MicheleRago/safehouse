from rest_framework import serializers
from safehouse.models import *

class SettoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Settore
        fields = "__all__"


class SensoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensore
        fields = "__all__"


class InfoSistemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = InfoSistema
        exclude = ('id', )


class FCMTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = FCMToken
        fields = "__all__"


class CodiceTemporaneoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CodiceTemporaneo
        fields = "__all__"