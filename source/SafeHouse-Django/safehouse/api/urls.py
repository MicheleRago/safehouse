from django.urls import path, include
from .views import *

urlpatterns = [
    path('info-sistema', InfoSistemaView.as_view(), name="info-sistema"),
    path('settori/', SettoreListCreateAPIView.as_view(), name="settore-list"),
    path('settori/by-id/<int:pk>', SettoreRetrieveUpdateDestroyAPIView.as_view(), name="settore"),
    path('sensori/', SensoreListCreateAPIView.as_view(), name="sensore-list"),
    path('sensori/by-id/<int:pk>', SensoreRetrieveUpdateDestroyAPIView.as_view(), name="sensore"),
    path('fcm-tokens/', FCMTokenListCreateAPIView.as_view(), name="fcm-token-list"),
    path('fcm-tokens/by-id/<str:pk>', FCMTokenRetrieveUpdateDestroyAPIView.as_view(), name="fcm-token"),
    path('codici-temporanei/', CodiceTemporaneoListCreateAPIView.as_view(), name="codici-temporanei-list"),
    path('codici-temporanei/by-id/<int:pk>', CodiceTemporaneoRetrieveUpdateDestroyAPIView.as_view(), name="codice-temporaneo"),
]