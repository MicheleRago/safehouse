import threading
import RPi.GPIO as GPIO
import time
from .models import InfoSistema

def manage_buzzer():
    """ 
    Metodo per gestire il buzzer in base alle condizione dello stato globale del sitema
    """
    triggerPIN = 17
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(triggerPIN,GPIO.OUT)
    buzzer = GPIO.PWM(triggerPIN, 1000)

    is_buzzer_on = False
    while True:
        infoSistema = InfoSistema.get_solo()
        if infoSistema.stato_globale == 2:
            if not is_buzzer_on:
                buzzer.start(10)
                is_buzzer_on = True
                time.sleep(5)
            else:
                buzzer.stop()
                is_buzzer_on = False
                time.sleep(5)
        elif infoSistema.stato_globale == 3:
            if not is_buzzer_on:
                buzzer.start(10)
                is_buzzer_on = True
        else:
            if is_buzzer_on:
                buzzer.stop()
                is_buzzer_on = False


def start():
    """
    Metodo che lancia il metodo manage_buzzer() in un nuovo thread 
    """
    t = threading.Thread(target=manage_buzzer, args=(), kwargs={})
    t.setDaemon(True)
    t.start()