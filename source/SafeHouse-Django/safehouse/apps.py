from django.apps import AppConfig
import os


class safehouseConfig(AppConfig):
    name = 'safehouse'

    def ready(self):
        if os.getenv("BUZZER_ENABLED", 'False').lower() in ('true', '1', 't'):
            from .buzzer import start
            start()
