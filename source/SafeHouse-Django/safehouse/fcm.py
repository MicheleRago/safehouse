from pyfcm import FCMNotification
from .models import InfoSistema, FCMToken

def send_intrusion_notification ():
    info_sistema = InfoSistema.get_solo()
    if info_sistema.stato_globale == 3:
        api_key = "AAAAVafn-eM:APA91bHkFTueITHl9j1RlYrCWN8AP7uSES2c_xUlyF8u2PbnaVECBPQYPbZ35RbWyLtzvfQ8mxBzBZqSeP8wmPK8f9FpcQjMlLzpdW-djJPHlOaqiifhE1OXCGTghmY8DxPuUWJpI5yS"
        push_service = FCMNotification(api_key = api_key)
        
        registration_ids = []
        fcm_tokens = FCMToken.objects.all()
        for fcm_token in fcm_tokens:
            registration_ids.append(fcm_token.token)

        message_title = "SafeHouse"
        message_body = "Intrusione individuata"
        result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title, message_body=message_body)

        print(result)
