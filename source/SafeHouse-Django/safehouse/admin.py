from django.contrib import admin
from .models import *
from solo.admin import SingletonModelAdmin


admin.site.register(Settore)
admin.site.register(Sensore)
admin.site.register(InfoSistema, SingletonModelAdmin)
admin.site.register(FCMToken)
admin.site.register(CodiceTemporaneo)