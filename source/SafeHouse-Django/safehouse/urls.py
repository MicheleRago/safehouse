from django.urls import path, include
from .views import *


urlpatterns = [
    path("api/", include('safehouse.api.urls')),
    path("", index),
    path("config", config),
    path("appmobile", app_mobile),
    path("settori", settori),
    path("settori-mod", settori_mod),
    path("codici", codici)
]