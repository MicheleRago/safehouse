import threading
import time
import json
from threading import Thread
from .models import *
from safehouse.mqtt import publish

class StoppableThread(Thread):
    def __init__(self, *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

def update_sectors():
    time.sleep(10)
    if threading.current_thread().stopped() == False:
        msg_body = {'active': []}
        settori = Settore.objects.all()
        sensori = Sensore.objects.all()
        for sens in sensori:
            if sens.settore.is_active == True:
                msg_body['active'].append(sens.codice)
        publish('/status', json.dumps(msg_body), 0)
        tList.clear()
    return

def get_tList():
    return tList

tList = []
