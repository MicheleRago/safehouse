from django.shortcuts import render
from .models import *
from random import randrange


def index (request):
    infosistema = InfoSistema.get_solo()
    if infosistema.stato_globale is None:
        infosistema.codice_proprietario = "{:06d}".format(randrange(999999))
        infosistema.codice_app_mobile = "{:06d}".format(randrange(999999))
        infosistema.save()
        return render(request, 'safehouse/start.html')
    else:
       return render(request, 'safehouse/index.html')


def config (request):
    return render(request, 'safehouse/config.html')


def app_mobile (request):
    return render(request, 'safehouse/appMobile.html')


def settori (request):
    return render(request, 'safehouse/settori.html')

def settori_mod (request):
    return render(request, 'safehouse/settori-mod.html')

def codici (request):
    return render(request, 'safehouse/codici.html')