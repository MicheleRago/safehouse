import json
import paho.mqtt.client as mqtt
from .models import *

TOPIC_HELLO = "/hello"
TOPIC_MOVIMENTO = "/movimento"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(TOPIC_HELLO)
    client.subscribe(TOPIC_MOVIMENTO)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    try:
        print(msg.topic+" "+str(msg.payload))

        if msg.topic == TOPIC_HELLO:
            handle_on_message_topic_hello(msg)
        elif msg.topic == TOPIC_MOVIMENTO:
            handle_on_message_topic_movimento(msg)
    except:
        print("Something went wrong")

def handle_on_message_topic_movimento(msg):
    infoSistema = InfoSistema.get_solo()
    if infoSistema.stato_globale == 1:
        msg = int(msg.payload.decode("utf-8"))
        codice = "{:03d}".format(msg)
        sensore = Sensore.objects.get(codice=codice)
        if sensore.settore.is_active:
            infoSistema.stato_globale = 2
            infoSistema.save()

def handle_on_message_topic_hello(msg):
    msg_json = json.loads(msg.payload.decode("utf-8"))
    sensori = Sensore.objects.all()
    if len(sensori) != 0:
        print("Errore Sensori presenti")
    else:
        for i in range(msg_json["sensorsNumber"]):
            codice = "{:03d}".format((i+1))
            Sensore.objects.create(codice=codice)

def publish(topic, payload, qos):
    print("MQTT: publish on topic ")
    (rc, mid) = client.publish(topic, payload, qos=qos)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("pippo","pluto")
client.connect("home.rago.ovh",1883, 60)
client.loop_start()