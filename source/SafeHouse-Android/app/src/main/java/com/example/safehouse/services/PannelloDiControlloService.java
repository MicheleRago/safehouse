package com.example.safehouse.services;

import com.example.safehouse.models.CodiceTemporaneo;
import com.example.safehouse.models.FCMToken;
import com.example.safehouse.models.InfoSistema;
import com.example.safehouse.models.Settore;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PannelloDiControlloService {
    @POST("fcm-tokens/")
    Call<FCMToken> postFCMToken(@Body FCMToken fcmtoken);

    @PUT("fcm-tokens/by-id/{id}")
    Call<FCMToken> putFCMToken(@Path("id")String id, @Body FCMToken fcmtoken);

    @GET("info-sistema")
    Call<InfoSistema> getInfoSistema();

    @GET("settori/")
    Call<List<Settore>> getSettori();

    @GET("codici-temporanei/")
    Call<List<CodiceTemporaneo>> getCodiciTemporanei();

    @POST("codici-temporanei/")
    Call<CodiceTemporaneo> postCodiceTemporaneo(@Body CodiceTemporaneo codiceTemporaneo);

    @DELETE("codici-temporanei/by-id/{id}")
    Call<ResponseBody> deleteCodiceTemporaneon(@Path("id")long id);

    @PUT("codici-temporanei/by-id/{id}")
    Call<CodiceTemporaneo> putCodiceTemporaneo(@Path("id")long id, @Body CodiceTemporaneo codiceTemporaneo);

    @PUT("settori/by-id/{id}")
    Call<Settore> putSettore(@Path("id")long id, @Body Settore settore);

}