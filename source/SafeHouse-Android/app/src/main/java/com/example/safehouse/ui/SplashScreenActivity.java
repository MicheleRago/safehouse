package com.example.safehouse.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.example.safehouse.R;
import com.example.safehouse.ui.ConfigurazioneActivity;
import com.example.safehouse.ui.MainActivity;
import com.example.safehouse.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setVersionText();
        selezionaActivityDaAprire();
    }

    /**
     * Il metodo selezionaActivityDaAprire viene utilizzato per selezionare e aprire l'activity
     * corretta.
     */
    private void selezionaActivityDaAprire() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREF_FILE, MODE_PRIVATE);
        if(sharedPref.contains(Constants.SHARED_PREF_SETUP_CONCLUDED)){
            openActivity(MainActivity.class);
        } else {
            openActivity(ConfigurazioneActivity.class);
        }
    }

    /**
     * Il metodo openActivity viene utilizzato per aprire l'activity successiva dopo un tempo
     * prestabilito, questo tempo serve per permettere all'utente di visualizzare le informazioni.
     * @param cls
     */
    private void openActivity(final Class<?> cls) {
        Timer mTimer = new Timer();
        mTimer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        Intent questIntent = new Intent(getApplicationContext(), cls);
                        startActivity(questIntent);
                        finish();
                    }
                },
                2000);
    }


    /**
     * Il metodo setVersionText viene utilizzato per settare la TextView relativa alla versione
     * dell'applicazione.
     */
    private void setVersionText() {
        TextView mVersionTextView = (TextView) findViewById(R.id.textVersionApp);
        try {
            PackageInfo mPackageInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String mVersion = mPackageInfo.versionName;
            mVersionTextView.setText(mVersionTextView.getText() + " " + mVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}