package com.example.safehouse.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.tbruyelle.rxpermissions3.RxPermissions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;

/**
 * La classe FileManager viene utilizzata per gestire i file.
 */
public class FileManager {

    /**
     * Il metodo checkFolder data la locazione di una cartella controlla se esiste. Nel caso in cui
     * non esista la crea.
     *
     * @param mDirectoryPath stringa che rappresenta la locazione di una cartella.
     */
    public static void checkFolder(String mDirectoryPath) {
        File mDirectory = new File(mDirectoryPath);
        if (!mDirectory.exists()) mDirectory.mkdirs();
    }

    /**
     * Il metodo checkNeededFolder controlla se le cartelle essenziali per la nostra applicazione
     * esistono, in caso contrario le crea adoperando il metodo checkFolder.
     *
     * @param context
     */
    public static void checkNeededFolders(Context context) {

        //checkFolder(context.getApplicationInfo().dataDir + Constants.TEMP_PATH);
    }

    /**
     * Il metodo writeFile crea un file contenente la stringa data.
     *
     * @param data stringa da salvare nel file.
     * @param path locazione del file a cui verrà scritto il dato.
     */
    public static void writeToFile(String data, String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Il metodo deleteFIle elimina un file.
     *
     * @param path stringa contenente la locazione del file da eliminare.
     */
    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * Il metodo existsFile verifica l'esistenza di un determinato file.
     *
     * @param path stringa contenente la locazione del file da verificare.
     * @return true se esiste il file, false altrimenti.
     */
    public static boolean existsFile(String path) {
        File file = new File(path);
        return file.exists();
    }

    /**
     * Il metodo checkStoragePermissions controlla se l'applicazione ha i permessi di archiviazione.
     *
     * @param activity
     * @return
     */
    public static void checkStoragePermissions(Activity activity) {
        RxPermissions rxPermissions = new RxPermissions((FragmentActivity) activity);
        rxPermissions
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        // I can control the camera now
                    } else {
                        // Oups permission denied
                    }
                });
    }

    /**
     * Il metodo saveObjectToFile dato un oggetto e una path di un file ti permette di creare un file
     * json alla path definita contenente le informazioni dell'oggetto datogli.
     *
     * @param object
     * @param path
     */
    public static void saveObjectToFile(Object object, String path) {
        String json = new Gson().toJson(object);
        writeToFile(json, path);
    }

    /**
     * Il metodo openObjectFromFIle data una classe e una path permetto di ottenere l'oggetto definito
     * nella path.
     *
     * @param path
     * @param cls
     * @return
     */
    public static Object openObjectFromFile(String path, Class<?> cls) {
        try {
            File file = new File(path);
            return new Gson().fromJson(new BufferedReader(new FileReader(file.getAbsolutePath())), cls);
        } catch (Exception ex) {
            Log.e("Exception", "openObjectToFile fail: " + ex.toString());
            return null;
        }
    }

    /**
     * Il metodo openObjectFromFIle data una stringa e un tip,o permette di ottenere l'oggetto definito
     * all'interno della path.
     *
     * @param path
     * @param cls
     * @return
     */
    public static Object openObjectFromFile(String path, Type cls) {
        try {
            File file = new File(path);
            return new Gson().fromJson(new BufferedReader(new FileReader(file.getAbsolutePath())), cls);
        } catch (Exception ex) {
            Log.e("Exception", "openObjectToFile fail: " + ex.toString());
            return null;
        }
    }

    /**
     * Il metodo downloadFile permettere di scaricare un file tramite la sua url e verrà in seguito
     * salvato nella posizione definita nella path.
     * @param url
     * @param path
     * @param progress
     * @return
     * @throws IOException
     */
    public static boolean downloadFile(String url, String path,
                                       MutableLiveData<Integer> progress) throws IOException {
        try {
            URL mUrl = new URL(url);
            URLConnection mUrlCon = mUrl.openConnection();
            mUrlCon.setConnectTimeout(5000);
            mUrlCon.setReadTimeout(5000);
            int contentLength = mUrlCon.getContentLength();
            InputStream is = mUrlCon.getInputStream();

            DataInputStream mDataInputStream = new DataInputStream(is);
            byte[] mBuffer = new byte[contentLength];
            int mLength;
            long total = 0;
            FileOutputStream mFileOutputStream = new FileOutputStream(new File( path ));

            while ((mLength = mDataInputStream.read(mBuffer))>0) {
                total += mLength;
                progress.postValue((int) (total*100/contentLength));
                mFileOutputStream.write(mBuffer, 0, mLength);
            }
            return true;
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Il metodo openFIle viene utilizzato per aprire un file che contiene un testo.
     * @param path
     * @param mime
     * @param context
     */
    public static void openFile(String path, String mime, Context context){

        File file = new File(path);
        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file );

        if (file.exists()) {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri , mime);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent chooser = Intent.createChooser(intent, "Open with");

            try {
                context.startActivity(chooser);
            }
            catch (ActivityNotFoundException e) {
                Log.e("error","error"+e);
            }
        }
    }
}

