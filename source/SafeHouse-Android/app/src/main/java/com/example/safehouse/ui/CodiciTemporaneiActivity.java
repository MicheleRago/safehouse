package com.example.safehouse.ui;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.safehouse.R;
import com.example.safehouse.adapter.CodiceTemporaneoAdapter;
import com.example.safehouse.dialog.LoadingDialog;
import com.example.safehouse.models.CodiceTemporaneo;
import com.example.safehouse.repository.PannelloDiControlloRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class CodiciTemporaneiActivity extends AppCompatActivity implements  PannelloDiControlloRepository.OnCallBackListener,  NavigationView.OnNavigationItemSelectedListener, CodiceTemporaneoAdapter.OnCardListener{
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    private LoadingDialog mLoadingDialog;
    private List<CodiceTemporaneo> listaCodiciTemporanei;
    private RecyclerView rv;
    private CodiceTemporaneoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codici_temporanei);

        mLoadingDialog = new LoadingDialog(this);
        mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        PannelloDiControlloRepository.getInstance(this).getCodiciTemporanei(this);
    }

    @Override
    public void onCallFinished(Object risultato) {
        if(risultato == null)
            onCallFail(getResources().getString(R.string.dialog_error));
        if (risultato instanceof List) {
            listaCodiciTemporanei = (List<CodiceTemporaneo>) risultato;
            setUi();
            mLoadingDialog.dismissDialog();
        } else if(risultato instanceof  CodiceTemporaneo) {
            listaCodiciTemporanei.remove((CodiceTemporaneo) risultato);
            adapter.notifyDataSetChanged();
            mLoadingDialog.dismissDialog();
        }
    }

    @Override
    public void onCallFail(String error) {
        mLoadingDialog.dismissDialog();
        new AlertDialog.Builder(this)
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_close, null)
                .show();
    }

    private void setUi() {
        setNavigationView();
        setFloatingActionButton();
        setRecyclerView();
    }

    /**
     * setRecyclerView è un metodo per impostare la RecyclerView rvCodiciTemporanei.
     */
    private void setRecyclerView() {
        rv = (RecyclerView)findViewById(R.id.rvCodiciTemporanei);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        Collections.sort(listaCodiciTemporanei);
        adapter = new CodiceTemporaneoAdapter(listaCodiciTemporanei, this, this);
        rv.setAdapter(adapter);
    }

    /**
     * setNavigationView è un metodo per impostare il navigation drawer.
     */
    private void setNavigationView() {
        drawerLayout = findViewById(R.id.my_drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Override di onOptionsItemSelected utilizzato per implementare l'apertura e la chiusura del
     * navigation drawer.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Override di onNavigationItemSelected utilizzato per gestire il click degli item del menu.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                Intent questIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(questIntent);
                finish();
                break;
            case R.id.nav_gestione_codici:
                drawerLayout.closeDrawers();
                break;
            case R.id.nav_gestione_settori:
                Intent questIntentS = new Intent(getApplicationContext(), SettoriActivity.class);
                startActivity(questIntentS);
                finish();
                break;
            default:
                break;
        }
        return false;
    }

    /**
     * Metodo utilizzato per gestire il FloatingActionButton.
     */
    private void setFloatingActionButton() {
        FloatingActionButton fab = findViewById(R.id.add_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent questIntent = new Intent(getApplicationContext(), CodiceTemporaneoActivity.class);
                questIntent.putExtra("listaCodiciTemporanei", (Serializable) listaCodiciTemporanei);
                someActivityResultLauncher.launch(questIntent);
            }
        });
    }

    /**
     * someActivityResultLauncher è un metodo utilizzato per gestire il ritorno dall'activity CodiceTemporaneoActivity.
     */
    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // È stato creato o modificato un codice temporaneo
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Aggiornamento della recyclerview
                        Intent data = result.getData();
                        listaCodiciTemporanei.clear();
                        listaCodiciTemporanei.addAll((List<CodiceTemporaneo>) data.getSerializableExtra("listaCodiciTemporanei"));
                        Collections.sort(listaCodiciTemporanei);
                        adapter.notifyDataSetChanged();
                    }
                }
            });


    @Override
    public void onCardClick(int position, int itemId) {
        if(itemId == R.id.imgDeleteCodice) {
            manageImageDeleteCodice(position);
        }
        else if(itemId == R.id.imgEditCodice) {
            manageImageEditCodice(position);
        }
    }

    /**
     * Metodo per gestire il click dell'ImageView imgDeleteCodice
     * @param position variabile contenente la posizione dell'elemento da eliminare nella lista listaCodiciTemporanei.
     */
    public void manageImageDeleteCodice(int position){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.eliminazione_codice));
        alert.setMessage(getString(R.string.eliminazione_codice_estesa));
        alert.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                PannelloDiControlloRepository.getInstance(getApplicationContext()).deleteCodiceTemporaneon(listaCodiciTemporanei.get(position), CodiciTemporaneiActivity.this);
            }
        });
        alert.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // close dialog
                dialog.cancel();
            }
        });
        alert.show();
    }

    /**
     * Metodo per gestire il click dell'ImageView imgEditCodice
     * @param position variabile contenente la posizione dell'elemento da modificare nella lista listaCodiciTemporanei.
     */
    public void manageImageEditCodice(int position) {
        Intent questIntent = new Intent(getApplicationContext(), CodiceTemporaneoActivity.class);
        questIntent.putExtra("listaCodiciTemporanei", (Serializable) listaCodiciTemporanei);
        questIntent.putExtra("codiceTemporaneo", (Serializable) listaCodiciTemporanei.get(position));

        someActivityResultLauncher.launch(questIntent);
    }
}