package com.example.safehouse.utils;

public class Constants {
    public static final String TEMP_PATH = "/tmp/";
    public static final String FILE_NAME_CONNECTION = "mConnection.json";
    public static final String SHARED_PREF_FILE = "com.example.safehouse.sheredPref";
    public static final String SHARED_PREF_SETUP_CONCLUDED = "setup_concluded";
}
