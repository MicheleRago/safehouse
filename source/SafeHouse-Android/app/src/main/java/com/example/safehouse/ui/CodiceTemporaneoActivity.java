package com.example.safehouse.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.safehouse.R;
import com.example.safehouse.dialog.LoadingDialog;
import com.example.safehouse.models.CodiceTemporaneo;
import com.example.safehouse.models.MessageQR;
import com.example.safehouse.models.Settore;
import com.example.safehouse.repository.PannelloDiControlloRepository;
import com.example.safehouse.utils.Constants;
import com.example.safehouse.utils.FileManager;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class CodiceTemporaneoActivity extends AppCompatActivity implements  PannelloDiControlloRepository.OnCallBackListener{

    private LoadingDialog mLoadingDialog;
    private List<Settore> listaSettori;
    private EditText mEditTextCodiceNome;
    private EditText mEditTextCodice;
    private EditText mEditTextCodiceScadenza;
    private LinearLayout mLinearLayoutSettori;
    private Calendar myCalendar= Calendar.getInstance();
    private List<CodiceTemporaneo> listaCodiciTemporanei;
    private CodiceTemporaneo nuovoCodiceTemporaneo;
    private CodiceTemporaneo oldCodiceTemporaneo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codice_temporaneo);

        mLoadingDialog = new LoadingDialog(this);
        mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        PannelloDiControlloRepository.getInstance(this).getSettori(this);
    }

    @Override
    public void onCallFinished(Object risultato) {
        if(risultato == null) {
            onCallFail(getResources().getString(R.string.dialog_error));
            return;
        }
        if(listaSettori == null) {
            listaSettori = (List<Settore>) risultato;
            setUi();
        }
        else if (oldCodiceTemporaneo==null){
            // Il codice è stato aggiunto correttamente
            listaCodiciTemporanei.add((CodiceTemporaneo)risultato);
            Intent intent = new Intent();
            intent.putExtra("listaCodiciTemporanei", (Serializable) listaCodiciTemporanei);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            // Il codice è stato modificato correttamente
            listaCodiciTemporanei.remove(oldCodiceTemporaneo);
            listaCodiciTemporanei.add((CodiceTemporaneo)risultato);
            Intent intent = new Intent();
            intent.putExtra("listaCodiciTemporanei", (Serializable) listaCodiciTemporanei);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
        mLoadingDialog.dismissDialog();
    }

    @Override
    public void onCallFail(String error) {
        mLoadingDialog.dismissDialog();
        new AlertDialog.Builder(this)
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_close, null)
                .show();
    }

    /**
     * Metodo utilizzato per iniziallizare gli oggetti grafici dell'activity.
     */
    private void setUi() {
        Intent i = getIntent();
        listaCodiciTemporanei = (List<CodiceTemporaneo>) i.getSerializableExtra("listaCodiciTemporanei");
        oldCodiceTemporaneo = (CodiceTemporaneo) i.getSerializableExtra("codiceTemporaneo");

        mEditTextCodiceNome = findViewById(R.id.txt_nome_codice);
        if(oldCodiceTemporaneo!=null)
            mEditTextCodiceNome.setText(oldCodiceTemporaneo.getNome());

        mEditTextCodice = findViewById(R.id.txt_codice);
        mEditTextCodiceScadenza = findViewById(R.id.txt_codice_scadenza);
        mLinearLayoutSettori = findViewById(R.id.linear_layout_settori);
        addSettoriCheckBox();
        setEditTextCodiceScadenza();
        setEditTextCodice();
        setBtnCodiceTemporaneo();
    }

    /**
     * Metodo utilizzato per aggiungere le checkbox dei settori.
     */
    private void addSettoriCheckBox() {
        for(Settore settore : listaSettori) {
            CheckBox cb = new CheckBox(getApplicationContext());
            cb.setTextColor(getResources().getColor(R.color.white));
            cb.setText(settore.getNome());
            cb.setButtonTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_color)));
            if(oldCodiceTemporaneo!=null && oldCodiceTemporaneo.getSettoriId().contains(settore.getId()))
                cb.setChecked(true);

            mLinearLayoutSettori.addView(cb);
        }
    }

    /**
     * Metodo utilizzato per gestire l'EditTextCodiceScadenza.
     */
    private void setEditTextCodiceScadenza() {
        if(oldCodiceTemporaneo!=null){
            String myFormat="dd/MM/yyyy";
            SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.ITALY);
            mEditTextCodiceScadenza.setText(dateFormat.format(oldCodiceTemporaneo.getScadenza()));
        }
        DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                String myFormat="dd/MM/yyyy";
                SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.ITALY);
                mEditTextCodiceScadenza.setText(dateFormat.format(myCalendar.getTime()));
            }
        };
        mEditTextCodiceScadenza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CodiceTemporaneoActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() +  24 * 60 * 60 * 1000);
                datePickerDialog.show();
            }
        });
    }

    /**
     * Metodo utilizzato per gestire l'EditTextCodice.
     */
    private void setEditTextCodice() {
        if(oldCodiceTemporaneo!=null){
            mEditTextCodice.setText(oldCodiceTemporaneo.getCodice());
        } else {
            MessageQR messageQR = (MessageQR) FileManager.openObjectFromFile(
                    this.getApplicationInfo().dataDir + Constants.TEMP_PATH + Constants.FILE_NAME_CONNECTION, MessageQR.class);
            Random rand = new Random();
            boolean trovato = true;
            String codice = null;
            do {
                int int_random = rand.nextInt(1000000);
                codice = String.format("%06d", int_random);
                if(messageQR.getmCodiceAppMobile().equals(codice))
                    trovato = false;
                for(CodiceTemporaneo codiceTemporaneo : listaCodiciTemporanei)
                    if(codiceTemporaneo.getCodice().equals(codice))
                        trovato = false;
            } while (!trovato);
            mEditTextCodice.setText(codice);
        }
    }

    /**
     * Metodo utilizzato per gestire il bottone.
     */
    private void setBtnCodiceTemporaneo() {
        Button btnCodiceTemporaneo = findViewById(R.id.btn_codice_temporaneo);
        if(oldCodiceTemporaneo != null)
            btnCodiceTemporaneo.setText(getString(R.string.applica));
        btnCodiceTemporaneo.setOnClickListener(new View.OnClickListener(){
            @Override
            //On click function
            public void onClick(View view) {
                String nome = mEditTextCodiceNome.getText().toString();
                if (TextUtils.isEmpty(nome)) {
                    Toast.makeText(CodiceTemporaneoActivity.this, getString(R.string.errore_codice_nome_vuoto), Toast.LENGTH_LONG).show();
                    return; // or break, continue, throw
                }
                boolean trovato = false;
                for(CodiceTemporaneo codiceTemporaneo : listaCodiciTemporanei)
                    if(codiceTemporaneo.getNome().equals(nome) && !oldCodiceTemporaneo.equals(codiceTemporaneo))
                        trovato = true;
                if(trovato) {
                    Toast.makeText(CodiceTemporaneoActivity.this, getString(R.string.errore_codice_nome_esistente), Toast.LENGTH_LONG).show();
                    return;
                }

                List<Long> listaSettoriSelezionati = new ArrayList<Long>();
                int count = mLinearLayoutSettori.getChildCount();
                for (int i = 0; i < count; i++) {
                    View v = mLinearLayoutSettori.getChildAt(i);
                    if (v instanceof CheckBox) {
                        CheckBox c = (CheckBox) v;
                        if(c.isChecked()) {
                            for(Settore settore : listaSettori)
                                if(settore.getNome().equals(c.getText().toString())) {
                                    listaSettoriSelezionati.add(settore.getId());
                                    break;
                                }
                        }
                    }
                }
                if(listaSettoriSelezionati.size()==0) {
                    Toast.makeText(CodiceTemporaneoActivity.this, getString(R.string.errore_settore_non_selezionato), Toast.LENGTH_LONG).show();
                    return;
                }

                Date scadenza = null;
                try {
                    scadenza = new SimpleDateFormat("dd/MM/yyyy").parse(mEditTextCodiceScadenza.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                    Toast.makeText(CodiceTemporaneoActivity.this, getString(R.string.errore_codice_data_scadenza), Toast.LENGTH_LONG).show();
                    return;
                }

                String codice = mEditTextCodice.getText().toString();
                nuovoCodiceTemporaneo = new CodiceTemporaneo(0, nome, scadenza, listaSettoriSelezionati, codice);

                mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                if(oldCodiceTemporaneo != null)
                    PannelloDiControlloRepository.getInstance(CodiceTemporaneoActivity.this).putCodiceTemporaneo(oldCodiceTemporaneo.getId(), nuovoCodiceTemporaneo,CodiceTemporaneoActivity.this);
                else
                    PannelloDiControlloRepository.getInstance(CodiceTemporaneoActivity.this).postCodiceTemporaneo(nuovoCodiceTemporaneo,CodiceTemporaneoActivity.this);
            }
        });
    }
}