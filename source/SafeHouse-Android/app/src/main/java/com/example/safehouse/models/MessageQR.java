package com.example.safehouse.models;

import com.google.gson.annotations.SerializedName;

public class MessageQR {
    @SerializedName(value = "address")
    private String mAddress;
    @SerializedName(value = "codiceAppMobile")
    private String mCodiceAppMobile;

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmCodiceAppMobile() {
        return mCodiceAppMobile;
    }

    public void setmCodiceAppMobile(String mCodiceAppMobile) {
        this.mCodiceAppMobile = mCodiceAppMobile;
    }
}
