package com.example.safehouse.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CodiceTemporaneo implements Serializable, Comparable<CodiceTemporaneo> {
    private long id;
    private String nome;
    private Date scadenza;
    @SerializedName(value = "settori")
    private List<Long> settoriId;
    private String codice;

    public CodiceTemporaneo(long id, String nome, Date scadenza, List<Long> settori, String codice) {
        this.id = id;
        this.nome = nome;
        this.scadenza = scadenza;
        this.settoriId = settori;
        this.codice = codice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    public List<Long> getSettoriId() {
        return settoriId;
    }

    public void setSettoriId(List<Long> settoriId) {
        this.settoriId = settoriId;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodiceTemporaneo that = (CodiceTemporaneo) o;
        return id == that.id && nome.equals(that.nome) && scadenza.equals(that.scadenza) && settoriId.equals(that.settoriId) && codice.equals(that.codice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, scadenza, settoriId, codice);
    }

    @Override
    public int compareTo(CodiceTemporaneo codiceTemporaneo) {
        if(codiceTemporaneo == null || getNome() == null || codiceTemporaneo.getNome() == null)
            return 0;
        return getNome().compareTo(codiceTemporaneo.getNome());
    }
}
