package com.example.safehouse.models;

import com.google.gson.annotations.SerializedName;

public class InfoSistema {
    @SerializedName(value = "stato_globale")
    private int statoGlobale;
    @SerializedName(value = "codice_app_mobile")
    private String codiceAppMobile;
    @SerializedName(value = "codice_proprietario")
    private String codiceProprietario;

    public int getStatoGlobale() {
        return statoGlobale;
    }

    public void setStatoGlobale(int statoGlobale) {
        this.statoGlobale = statoGlobale;
    }

    public String getCodiceAppMobile() {
        return codiceAppMobile;
    }

    public void setCodiceAppMobile(String codiceAppMobile) {
        this.codiceAppMobile = codiceAppMobile;
    }

    public String getCodiceProprietario() {
        return codiceProprietario;
    }

    public void setCodiceProprietario(String codiceProprietario) {
        this.codiceProprietario = codiceProprietario;
    }
}
