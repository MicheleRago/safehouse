package com.example.safehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.safehouse.R;
import com.example.safehouse.models.CodiceTemporaneo;

import java.util.Calendar;
import java.util.List;

public class CodiceTemporaneoAdapter  extends RecyclerView.Adapter<CodiceTemporaneoAdapter.CodiceTemporaneoViewHolder>{
    private List<CodiceTemporaneo> mListaCodiciTemporanei;
    private OnCardListener mOnCardListener;
    private Context mContext;

    public CodiceTemporaneoAdapter(List<CodiceTemporaneo> mListaCodiciTemporanei, OnCardListener onCardListener, Context context) {
        this.mListaCodiciTemporanei = mListaCodiciTemporanei;
        this.mOnCardListener = onCardListener;
        this.mContext = context;
    }

    @NonNull
    @Override
    public CodiceTemporaneoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_codice_temporaneo, parent, false);
        return new CodiceTemporaneoViewHolder(mView,mOnCardListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CodiceTemporaneoViewHolder holder, int position) {
        holder.mNomeCodice.setText(mListaCodiciTemporanei.get(position).getNome());
        Calendar calendar = Calendar.getInstance();

        if(mListaCodiciTemporanei.get(position).getScadenza().compareTo(calendar.getTime())>0)
            holder.mImgStatoCodice.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_dot_green));
        else
            holder.mImgStatoCodice.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_dot_red));

    }

    @Override
    public int getItemCount() {
        return mListaCodiciTemporanei.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class CodiceTemporaneoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView mCardView;
        TextView mNomeCodice;
        ImageView mImgDeleteCodice;
        ImageView mImgEditCodice;
        ImageView mImgStatoCodice;
        OnCardListener mOnCardListener;


        public CodiceTemporaneoViewHolder(@NonNull View itemView, OnCardListener mOnCardListener) {
            super(itemView);
            mCardView = (CardView)itemView.findViewById(R.id.codiceTemporaneoCardView);
            mNomeCodice = (TextView)itemView.findViewById(R.id.txtNomeCodiceTemporaneo);
            mImgDeleteCodice = (ImageView)itemView.findViewById(R.id.imgDeleteCodice);
            mImgEditCodice = (ImageView)itemView.findViewById(R.id.imgEditCodice);
            mImgStatoCodice = (ImageView)itemView.findViewById(R.id.imgStatoCodice);
            this.mOnCardListener = mOnCardListener;
            mImgDeleteCodice.setOnClickListener(this);
            mImgEditCodice.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnCardListener.onCardClick(getAdapterPosition(), v.getId());
        }
    }

    public interface OnCardListener{
        void onCardClick(int position, int itemId);
    }
}
