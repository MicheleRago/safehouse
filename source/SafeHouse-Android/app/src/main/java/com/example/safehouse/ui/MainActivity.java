package com.example.safehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.safehouse.R;
import com.example.safehouse.models.InfoSistema;
import com.example.safehouse.repository.PannelloDiControlloRepository;
import com.google.android.material.navigation.NavigationView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * La classe MainActivity viene utilizzata per visualizzare lo stato dell'allarme della casa,
 * se attivo o disattivo e in caso avere la possibilità di passare alla gestione degli allarmi.
 */
public class MainActivity extends AppCompatActivity  implements PannelloDiControlloRepository.OnCallBackListener,  NavigationView.OnNavigationItemSelectedListener {

    private Button mStatoButton;
    private TextView mStatoAllarme;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStatoButton = (Button)findViewById(R.id.button_stato);
        mStatoAllarme = (TextView) findViewById(R.id.statoAllarme);
        setNavigationView();

        startPollingAPI();
    }

    /**
     * setNavigationView è un metodo per impostare il navigation drawer.
     */
    private void setNavigationView() {
        drawerLayout = findViewById(R.id.my_drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Override di onOptionsItemSelected utilizzato per implementare l'apertura e la chiusura del
     * navigation drawer.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Override di onNavigationItemSelected utilizzato per gestire il click degli item del menu.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                drawerLayout.closeDrawers();
                break;
            case R.id.nav_gestione_codici:
                Intent questIntent = new Intent(getApplicationContext(), CodiciTemporaneiActivity.class);
                startActivity(questIntent);
                finish();
                break;
            case R.id.nav_gestione_settori:
                Intent questIntentS = new Intent(getApplicationContext(), SettoriActivity.class);
                startActivity(questIntentS);
                finish();
                break;
            default:
                break;
        }
        return false;
    }


    private void startPollingAPI(){
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PannelloDiControlloRepository.getInstance(MainActivity.this).getInfoSistema(MainActivity.this);
                    }
                });
            }
        },0,7000);
    }

    @Override
    public void onCallFinished(Object risultato) {
        if(risultato != null){
            switch(((InfoSistema)risultato).getStatoGlobale()){
                case 0:
                    mStatoAllarme.setText(R.string.stato_disattivo);
                    break;
                case 1:
                    mStatoAllarme.setText(R.string.stato_attivo);
                    break;
                case 2:
                    mStatoAllarme.setText(R.string.stato_movimento);
                    break;
                case 3:
                    mStatoAllarme.setText(R.string.stato_intrusione);
                    break;
                default:
                    mStatoAllarme.setText(R.string.stato_allarme);
                    break;
            }
        }
    }

    @Override
    public void onCallFail(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}