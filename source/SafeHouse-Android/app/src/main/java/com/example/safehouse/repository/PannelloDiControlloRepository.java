package com.example.safehouse.repository;

import android.content.Context;

import com.example.safehouse.models.CodiceTemporaneo;
import com.example.safehouse.models.FCMToken;
import com.example.safehouse.models.InfoSistema;
import com.example.safehouse.models.MessageQR;
import com.example.safehouse.models.Settore;
import com.example.safehouse.services.PannelloDiControlloService;
import com.example.safehouse.utils.Constants;
import com.example.safehouse.utils.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PannelloDiControlloRepository {
    private static PannelloDiControlloRepository instance;
    private PannelloDiControlloService mPannelloDiControlloService;

    private PannelloDiControlloRepository(Context context) {
        MessageQR messageQR = (MessageQR) FileManager.openObjectFromFile(
                context.getApplicationInfo().dataDir + Constants.TEMP_PATH + Constants.FILE_NAME_CONNECTION, MessageQR.class);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(messageQR.getmAddress() + "/api/").addConverterFactory(GsonConverterFactory.create(gson)).build();
        mPannelloDiControlloService = retrofit.create(PannelloDiControlloService.class);
    }

    public static synchronized PannelloDiControlloRepository getInstance (Context context){
        if(instance == null)
            instance = new PannelloDiControlloRepository(context);
        return instance;
    }

    /**
     * Metodo utilizzato per effettuare una post di un FCM token al server.
     * @param fcmToken
     * @param onCallBackListener
     */
    public void postFCMToken (FCMToken fcmToken, OnCallBackListener onCallBackListener){
        Call<FCMToken> call = mPannelloDiControlloService.postFCMToken(fcmToken);
        call.enqueue(
                new Callback<FCMToken>() {
                    @Override
                    public void onResponse(Call<FCMToken> call, Response<FCMToken> response) {
                        onCallBackListener.onCallFinished(response.body());
                    }

                    @Override
                    public void onFailure(Call<FCMToken> call, Throwable t) {
                        onCallBackListener.onCallFail(t.getMessage());
                    }
                });
    }

    /**
     * Metodo utilizzato per effettuare una put di uno specifico FCM token al server.
     * @param fcmToken
     * @throws IOException
     */
    public void putFCMTokenSync(FCMToken fcmToken) throws IOException {
        Call<FCMToken> call = mPannelloDiControlloService.putFCMToken(fcmToken.getId(),fcmToken);
        call.execute();
    }

    /**
     * Metodo utilizzato per ottenere un oggetto di tipo InfoSistema dal server.
     * @param onCallBackListener
     */
    public void getInfoSistema(OnCallBackListener onCallBackListener){
        Call<InfoSistema> call = mPannelloDiControlloService.getInfoSistema();
        call.enqueue(new Callback<InfoSistema>() {
            @Override
            public void onResponse(Call<InfoSistema> call, Response<InfoSistema> response) {
                onCallBackListener.onCallFinished(response.body());
            }

            @Override
            public void onFailure(Call<InfoSistema> call, Throwable t) {
                onCallBackListener.onCallFail(t.getMessage());
            }
        });
    }

    /**
     * Metodo utilizzato per ottenere la lista dei settori salvati sul server.
     * @param onCallBackListener
     */
    public void getSettori(OnCallBackListener onCallBackListener){
        Call<List<Settore>> call = mPannelloDiControlloService.getSettori();
        call.enqueue(new Callback<List<Settore>>() {
            @Override
            public void onResponse(Call<List<Settore>> call, Response<List<Settore>> response) {
                onCallBackListener.onCallFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<Settore>> call, Throwable t) {
                onCallBackListener.onCallFail(t.getMessage());
            }
        });
    }

    /**
     * Metodo utilizzato per ottenere la lista dei codici temporanei salvati sul server.
     * @param onCallBackListener
     */
    public void getCodiciTemporanei(OnCallBackListener onCallBackListener){
        Call<List<CodiceTemporaneo>> call = mPannelloDiControlloService.getCodiciTemporanei();
        call.enqueue(new Callback<List<CodiceTemporaneo>>() {
            @Override
            public void onResponse(Call<List<CodiceTemporaneo>> call, Response<List<CodiceTemporaneo>> response) {
                onCallBackListener.onCallFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<CodiceTemporaneo>> call, Throwable t) {
                onCallBackListener.onCallFail(t.getMessage());
            }
        });
    }

    /**
     * Metodo utilizzato per effettuare una post di un CodiceTemporaneo al server.
     * @param codiceTemporaneo
     * @param onCallBackListener
     */
    public void postCodiceTemporaneo (CodiceTemporaneo codiceTemporaneo, OnCallBackListener onCallBackListener){
        Call<CodiceTemporaneo> call = mPannelloDiControlloService.postCodiceTemporaneo(codiceTemporaneo);
        call.enqueue(
                new Callback<CodiceTemporaneo>() {
                    @Override
                    public void onResponse(Call<CodiceTemporaneo> call, Response<CodiceTemporaneo> response) {
                        onCallBackListener.onCallFinished(response.body());
                    }

                    @Override
                    public void onFailure(Call<CodiceTemporaneo> call, Throwable t) {
                        onCallBackListener.onCallFail(t.getMessage());
                    }
                });
    }

    /**
     * Metodo utilizzato per effettuare una delete di un CodiceTemporaneo al server.
     * @param codiceTemporaneo
     * @param onCallBackListener
     */
    public void deleteCodiceTemporaneon(CodiceTemporaneo codiceTemporaneo, OnCallBackListener onCallBackListener){
        Call<ResponseBody> call = mPannelloDiControlloService.deleteCodiceTemporaneon(codiceTemporaneo.getId());
        call.enqueue(
                new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        onCallBackListener.onCallFinished(codiceTemporaneo);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onCallBackListener.onCallFail(t.getMessage());
                    }
                });
    }

    /**
     * Metodo utilizzato per effettuare una put di un CodiceTemporaneo al server.
     * @param id
     * @param codiceTemporaneo
     * @param onCallBackListener
     */
    public void putCodiceTemporaneo(long id, CodiceTemporaneo codiceTemporaneo, OnCallBackListener onCallBackListener) {
        Call<CodiceTemporaneo> call = mPannelloDiControlloService.putCodiceTemporaneo(id, codiceTemporaneo);
        call.enqueue(
                new Callback<CodiceTemporaneo>() {
                    @Override
                    public void onResponse(Call<CodiceTemporaneo> call, Response<CodiceTemporaneo> response) {
                        onCallBackListener.onCallFinished(response.body());
                    }

                    @Override
                    public void onFailure(Call<CodiceTemporaneo> call, Throwable t) {
                        onCallBackListener.onCallFail(t.getMessage());
                    }
                });
    }

    /**
     * Metodo utilizzato per effettuarte una put di un Settore al server.
     * @param id
     * @param settore
     * @param onCallBackListener
     */
    public void putSettore(long id, Settore settore, OnCallBackListener onCallBackListener){
        Call<Settore> call = mPannelloDiControlloService.putSettore(id, settore);
        call.enqueue(
                new Callback<Settore>() {
                    @Override
                    public void onResponse(Call<Settore> call, Response<Settore> response) {
                        onCallBackListener.onCallFinished(response.body());
                    }

                    @Override
                    public void onFailure(Call<Settore> call, Throwable t) {
                        onCallBackListener.onCallFail(t.getMessage());
                    }
                });
    }


    /**
     * L'interfaccia OnCallBackListener serve per effettuare un pattern listener, utilizzato
     * nelle chiamate POST.
     */
    public interface OnCallBackListener {
        public void onCallFinished(Object risultato);

        public void onCallFail(String error);
    }
}
