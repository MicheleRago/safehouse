package com.example.safehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.safehouse.R;
import com.example.safehouse.adapter.SettoriAdapter;
import com.example.safehouse.dialog.LoadingDialog;
import com.example.safehouse.models.Settore;
import com.example.safehouse.repository.PannelloDiControlloRepository;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * La classe Settori activity viene utilizzata per la visualizzazione dello stato dei settori.
 */
public class SettoriActivity extends AppCompatActivity implements  PannelloDiControlloRepository.OnCallBackListener, NavigationView.OnNavigationItemSelectedListener{

    private RecyclerView mRvSettoriAttivi;
    private RecyclerView mRvSettoriDisattivi;
    private List<Settore> mListaSettori;
    private List<Settore> mListaSettoriAttivi;
    private List<Settore> mListaSettoriDisattivi;
    private List<Settore> mListaSettoriDaModificare;
    private LoadingDialog mLoadingDialog;
    private SettoriAdapter mAdapterSettoriAttivi;
    private SettoriAdapter mAdapterSettoriDisattivi;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private CheckBox mCbAttivi;
    private CheckBox mCbDisattivi;
    private Button mBtnApplica;

    @Override

    /**
     * Il metodo onCreate viene utilizzato alla creazione dell'activity
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settori);

        mLoadingDialog = new LoadingDialog(this);
        mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        PannelloDiControlloRepository.getInstance(this).getSettori(this);
    }

    @Override
    public void onCallFinished(Object risultato) {
        if(risultato == null) {
            onCallFail(getResources().getString(R.string.dialog_error));
        }else if (risultato instanceof List) {
            mListaSettori = (List<Settore>) risultato;
            setRecyclerViewSettori();
            setCheckboxes();
            setButton();
            setNavigationView();
            mLoadingDialog.dismissDialog();
        }else if(mListaSettoriDaModificare.size()>1){
            updateListaSettori(mListaSettoriDaModificare.get(0));
            mListaSettoriDaModificare.remove(0);
            PannelloDiControlloRepository.getInstance(SettoriActivity.this)
                    .putSettore(mListaSettoriDaModificare.get(0).getId(),mListaSettoriDaModificare.get(0),SettoriActivity.this);
        }else if (mListaSettoriDaModificare.size()==1){
            updateListaSettori(mListaSettoriDaModificare.get(0));
            mListaSettoriDaModificare.remove(0);
            updateRecyclerViewSettori();
            mLoadingDialog.dismissDialog();
        }
    }

    @Override
    public void onCallFail(String error) {
        mLoadingDialog.dismissDialog();
        new AlertDialog.Builder(this)
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_close, null)
                .show();
    }

    /**
     * Il metodo updateListaSettori viene utilizzato per aggiornare un settore presente nella lista
     * mListaSettori.
     * @param settore
     */
    public void updateListaSettori(Settore settore){
        for(int i=0; i<mListaSettori.size();i++){
            if(mListaSettori.get(i).getId() == settore.getId()){
                mListaSettori.get(i).setIs_active(settore.getIs_active());
                break;
            }
        }
    }

    /**
     * Il metodo updateRecycleViewSettori viene utilizzato per aggiornare le recucler view.
     */
    public void updateRecyclerViewSettori(){
        mListaSettoriAttivi.clear();
        mListaSettoriAttivi.addAll(getSettoriByStato(true));
        mAdapterSettoriAttivi.notifyDataSetChanged();
        mListaSettoriDisattivi.clear();
        mListaSettoriDisattivi.addAll(getSettoriByStato(false));
        mAdapterSettoriDisattivi.notifyDataSetChanged();
        setCheckboxes();
    }

    /**
     * Il metodo getSettoriByStato viene ulizzato per creare una lista in base al parametro di stato
     * passato.
     * @param stato variabile utillizzata per filtrare i vari settori
     * @return
     */
    private List<Settore> getSettoriByStato(boolean stato){
        List <Settore> listaTemp = new ArrayList<Settore>();
        for(int i=0; i < mListaSettori.size(); i++)
            if(mListaSettori.get(i).getIs_active()==stato)
                listaTemp.add(mListaSettori.get(i));
        return listaTemp;
    }

    /**
     * Il metodo setRecyclerViewSettori viene utilizzato per settare correttamente le recyclerView.
     */
    private void setRecyclerViewSettori() {
        mRvSettoriAttivi = (RecyclerView)findViewById(R.id.rvCardSettoriArrivi);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRvSettoriAttivi.setLayoutManager(llm);
        //Collections.sort(mListaSettori);
        mListaSettoriAttivi = getSettoriByStato(true);
        mAdapterSettoriAttivi = new SettoriAdapter(mListaSettoriAttivi, this);
        mRvSettoriAttivi.setAdapter(mAdapterSettoriAttivi);

        mRvSettoriDisattivi = (RecyclerView)findViewById(R.id.rvCardSettoriDisattivi);
        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        mRvSettoriDisattivi.setLayoutManager(llm2);
        //Collections.sort(mListaSettori);
        mListaSettoriDisattivi = getSettoriByStato(false);
        mAdapterSettoriDisattivi = new SettoriAdapter(mListaSettoriDisattivi, this);
        mRvSettoriDisattivi.setAdapter(mAdapterSettoriDisattivi);
    }

    /**
     * Il metodo setCHeckboxes viene utilizzato per impostare le checkboxes "Seleziona tutto".
     */
    private void setCheckboxes(){
        mCbAttivi = findViewById(R.id.cbAttivi);
        mCbDisattivi = findViewById(R.id.cbDisattivi);
        if(mListaSettoriAttivi.size() > 0){
            mCbAttivi.setVisibility(View.VISIBLE);
            mCbAttivi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickCheckboxes(view);
                }
            });
        }else{
            mCbAttivi.setVisibility(View.GONE);
        }
        mCbAttivi.setChecked(false);
        if(mListaSettoriDisattivi.size() > 0){
            mCbDisattivi.setVisibility(View.VISIBLE);
            mCbDisattivi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickCheckboxes(view);
                }
            });
        }else{
            mCbDisattivi.setVisibility(View.GONE);
        }
        mCbDisattivi.setChecked(false);
    }

    /**
     * Il metodo onClickCheckboxes viene utilizzato per gestire il click delle checkboxes "Seleziona tutto".
     * @param view
     */
    private void onClickCheckboxes(View view){
        List<Settore>listaSettori = null;
        RecyclerView rv = null;
        if(view.getId()==R.id.cbAttivi){
            listaSettori = mListaSettoriAttivi;
            rv = mRvSettoriAttivi;
            }else {
            listaSettori = mListaSettoriDisattivi;
            rv = mRvSettoriDisattivi;
        }
        CheckBox c = (CheckBox) view;
        for(int i=0;i< listaSettori.size();i++)
            ((MaterialCheckBox)((ViewGroup)((ViewGroup)((ViewGroup)rv.getLayoutManager()
                    .getChildAt(i)).getChildAt(0)).getChildAt(0)).getChildAt(0)).setChecked(c.isChecked());
    }

    /**
     * Il metodo setButton viene utilizzato per gestire il click del bottone.
     */
    public void setButton(){
        mBtnApplica = findViewById(R.id.btnApplica);
        mBtnApplica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListaSettoriDaModificare = new ArrayList<Settore>();
                boolean flag = false;
                for(int i=0;i< mListaSettoriAttivi.size();i++){
                    if(((MaterialCheckBox)((ViewGroup)((ViewGroup)((ViewGroup)mRvSettoriAttivi.getLayoutManager()
                            .getChildAt(i)).getChildAt(0)).getChildAt(0)).getChildAt(0)).isChecked()) {
                        if(!flag)
                            flag = true;
                        Settore settore = mListaSettoriAttivi.get(i);
                        settore.setIs_active(!settore.getIs_active());
                        mListaSettoriDaModificare.add(settore);
                    }
                }

                for(int i=0;i< mListaSettoriDisattivi.size();i++){
                    if(((MaterialCheckBox)((ViewGroup)((ViewGroup)((ViewGroup)mRvSettoriDisattivi.getLayoutManager()
                            .getChildAt(i)).getChildAt(0)).getChildAt(0)).getChildAt(0)).isChecked()) {
                        if(!flag)
                            flag = true;
                        Settore settore = mListaSettoriDisattivi.get(i);
                        settore.setIs_active(!settore.getIs_active());
                        mListaSettoriDaModificare.add(settore);
                    }
                }
                if(flag){
                    mLoadingDialog.startDialog(getString(R.string.dialog_loading_generic), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    PannelloDiControlloRepository.getInstance(SettoriActivity.this).putSettore(mListaSettoriDaModificare.get(0).getId(),mListaSettoriDaModificare.get(0),SettoriActivity.this);
                }

            }
        });
    }


    /**
     * setNavigationView è un metodo per impostare il navigation drawer.
     */
    private void setNavigationView() {
        drawerLayout = findViewById(R.id.drawer_layout_settori);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navigation_view_settori);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }

        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Override di onOptionsItemSelected utilizzato per implementare l'apertura e la chiusura del
     * navigation drawer.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Override di onNavigationItemSelected utilizzato per gestire il click degli item del menu.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                Intent questIntentHome = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(questIntentHome);
                finish();
                break;
            case R.id.nav_gestione_codici:
                Intent questIntent = new Intent(getApplicationContext(), CodiciTemporaneiActivity.class);
                startActivity(questIntent);
                finish();
                break;
            case R.id.nav_gestione_settori:
                drawerLayout.closeDrawers();
                break;
            default:
                break;
        }
        return false;
    }
}