package com.example.safehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.example.safehouse.R;
import com.example.safehouse.models.FCMToken;
import com.example.safehouse.models.MessageQR;
import com.example.safehouse.repository.PannelloDiControlloRepository;
import com.example.safehouse.utils.Constants;
import com.example.safehouse.utils.FileManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.zxing.Result;
import com.tbruyelle.rxpermissions3.RxPermissions;

import java.util.Objects;

/**
 * La classe ConfigurazioneActivity viene utilizzata al primo avvio dell'applicazione
 * in cui l'utente dovrà associare l'applicazione al pannello di controllo.
 */
public class ConfigurazioneActivity extends AppCompatActivity implements PannelloDiControlloRepository.OnCallBackListener{

    private FCMToken fcmToken;
    private CodeScanner mCodeScanner;
    private boolean isTokenRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurazione);
        checkCameraAndStoragePermissions();
        setCodeScanner();
    }

    /**
     * Il metodo checkCameraPermission viene utilizzato per richiedere i permessi all'utente
     * per accedere alla videocamera.
     */
    private void checkCameraAndStoragePermissions(){
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions
                .request(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        // I can control the camera now
                    } else {
                        // Oups permission denied
                    }
                });
    }

    /**
     * Il metodo setCodeScanner viene utilizzato per inizializzare il CodeScanner.
     */
    private void setCodeScanner(){
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(ConfigurazioneActivity.this, result.getText(), Toast.LENGTH_SHORT).show();
                        if(!isTokenRunning) {
                            try {
                                Gson gson = new Gson();
                                MessageQR mMessageQR = gson.fromJson(result.getText(), MessageQR.class);
                                FileManager.checkFolder(ConfigurazioneActivity.this.getApplicationInfo().dataDir + Constants.TEMP_PATH);
                                FileManager.writeToFile(result.getText(),
                                        ConfigurazioneActivity.this.getApplicationInfo().dataDir + Constants.TEMP_PATH + Constants.FILE_NAME_CONNECTION);
                                isTokenRunning = true;
                                getFirebaseMessagingToken();
                            } catch (JsonParseException e) {
                                Log.e("SafeHouse", "exception", e);
                            }
                        }
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }
    public void getFirebaseMessagingToken ( ) {
        FirebaseMessaging.getInstance ().getToken ()
                .addOnCompleteListener ( task -> {
                    if (!task.isSuccessful ()) {
                        //Could not get FirebaseMessagingToken
                        isTokenRunning = false;
                        return;
                    }
                    if (null != task.getResult ()) {
                        //Got FirebaseMessagingToken
                        String firebaseMessagingToken = Objects.requireNonNull ( task.getResult () );
                        fcmToken = new FCMToken();
                        fcmToken.setToken(firebaseMessagingToken);
                        getFirebaseMessaggingId();
                    }
                } );
    }

    public void getFirebaseMessaggingId(){
        FirebaseInstallations.getInstance().getId()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (task.isSuccessful()) {
                            Log.d("Installations", "Installation ID: " + task.getResult());
                            fcmToken.setId(task.getResult());
                            PannelloDiControlloRepository.getInstance(ConfigurazioneActivity.this).postFCMToken(fcmToken, ConfigurazioneActivity.this);
                        } else {
                            Log.e("Installations", "Unable to get Installation ID");
                            isTokenRunning = false;
                        }
                    }
                });
    }

    /**
     * Il metodo saveOnSharePref viene utilizzato per salvare all'interno delle sharedPreferences
     * il fatto che sia terminata la fase di primo avvio dell'applicazione.
     */
    private void saveOnSharePref() {
        SharedPreferences sharedPref =
                getSharedPreferences(Constants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(Constants.SHARED_PREF_SETUP_CONCLUDED, true);
        editor.apply();
    }


    @Override
    public void onCallFinished(Object result){
        if(result == null)
            isTokenRunning = false;
        else {
            saveOnSharePref();
            startActivity(new Intent(ConfigurazioneActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onCallFail(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        isTokenRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}