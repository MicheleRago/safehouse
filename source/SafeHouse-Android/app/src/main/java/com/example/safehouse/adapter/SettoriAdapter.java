package com.example.safehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.safehouse.R;
import com.example.safehouse.models.Settore;

import java.util.List;

public class SettoriAdapter extends RecyclerView.Adapter<SettoriAdapter.SettoriViewHolder> {
    private List<Settore> mListaSettori;
    private Context mContext;

    public SettoriAdapter(List<Settore> mListaSettori, Context context) {
        this.mListaSettori = mListaSettori;
        this.mContext = context;
    }

    @NonNull
    @Override
    public SettoriViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_settore, parent, false);
        return new SettoriAdapter.SettoriViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull SettoriViewHolder holder, int position) {
        if(mListaSettori.get(position).getIs_active())
            holder.txtStatoAllarmeCard.setText(mContext.getString(R.string.stato_attivo));
        else
            holder.txtStatoAllarmeCard.setText(mContext.getString(R.string.stato_disattivo));
        holder.txtNomeSettoreCard.setText(mListaSettori.get(position).getNome());
        holder.checkBoxCardSettori.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return mListaSettori.size();
    }


    public class SettoriViewHolder extends RecyclerView.ViewHolder {

        CardView cardViewSettori;
        CheckBox checkBoxCardSettori;
        TextView txtNomeSettoreCard;
        TextView txtStatoAllarmeCard;


        public SettoriViewHolder(@NonNull View itemView) {
            super(itemView);
            cardViewSettori = (CardView)itemView.findViewById(R.id.cardViewSettore);
            checkBoxCardSettori = (CheckBox)itemView.findViewById(R.id.checkBoxCardSettore);
            txtNomeSettoreCard = (TextView)itemView.findViewById(R.id.txtNomeSettoreCard);
            txtStatoAllarmeCard = (TextView)itemView.findViewById(R.id.txtStatoAllarmeCard);
        }
    }
}
