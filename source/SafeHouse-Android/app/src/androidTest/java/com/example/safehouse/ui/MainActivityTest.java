package com.example.safehouse.ui;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static com.example.safehouse.Utils.childAtPosition;
import static org.hamcrest.Matchers.allOf;

import android.os.SystemClock;
import android.widget.TextView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.safehouse.R;

import org.junit.Rule;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    /**
     * textViewStatoAllarmeTest è un metodo utilizzato per testare il corretto funzionamento
     * della textView della mainActivity contenente lo stato dell'allarme
     */
    @Test
    public void textViewStatoAllarmeTest() {
        SystemClock.sleep(5000);
        TextView mStatoAllarme = (TextView) mActivityTestRule.getActivity().findViewById(R.id.statoAllarme);
        List <String> allowedValues = Arrays.asList("ATTIVO","DISATTIVO","MOVIMENTO RILEVATO","INTRUSIONE RILEVATA");
        Assert.assertTrue(allowedValues.contains(mStatoAllarme.getText()));
    }

    /**
     * openMenuTest è un metodo utilizzato per testare la corretta apertura del navigation drawer.
     */
    @Test
    public void openMenuTest() {
        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Apri"),
                        withParent(allOf(withId(androidx.appcompat.R.id.action_bar),
                                withParent(withId(androidx.appcompat.R.id.action_bar_container)))),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Apri"),
                        childAtPosition(
                                allOf(withId(androidx.appcompat.R.id.action_bar),
                                        childAtPosition(
                                                withId(androidx.appcompat.R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        mActivityTestRule.getActivity().findViewById(R.id.navigation_view);

        DrawerLayout mDrawerLayout = (DrawerLayout) mActivityTestRule.getActivity().findViewById(R.id.my_drawer_layout);
        Assert.assertTrue(mDrawerLayout.isDrawerOpen(GravityCompat.START));
    }
}
