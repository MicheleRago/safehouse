package com.example.safehouse.ui;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.example.safehouse.Utils.childAtPosition;
import static com.example.safehouse.Utils.clickChildViewWithId;
import static com.example.safehouse.Utils.getActivityInstance;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.safehouse.R;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Random;

@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class SettoriActivityTest {

    @Rule
    public ActivityTestRule<SettoriActivity> mActivityTestRule = new ActivityTestRule<SettoriActivity>(SettoriActivity.class);

    /**
     * Test per verifica la presenza di una card settori e il corretto funzionamento della pressione.
     * di una checkbox
     */
    @Test
    public void stage1_testCheckboxCard(){
        onView(withId(R.id.rvCardSettoriDisattivi)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.checkBoxCardSettore)));
    }

    /**
     * Test per verificare il corretto funzionamento della modifica di un settore.
     */
    @Test
    public void stage2_modicaSettori(){
        onView(withId(R.id.rvCardSettoriDisattivi)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.checkBoxCardSettore)));
        ViewInteraction materialButton = onView(
                allOf(withId(R.id.btnApplica), withText("APPLICA"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.linearLayout2),
                                        4),
                                0),
                        isDisplayed()));
        materialButton.perform(click());
    }
}
