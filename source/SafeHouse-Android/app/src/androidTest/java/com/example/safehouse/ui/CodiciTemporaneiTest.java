package com.example.safehouse.ui;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.example.safehouse.Utils.childAtPosition;
import static com.example.safehouse.Utils.clickChildViewWithId;
import static com.example.safehouse.Utils.getActivityInstance;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.safehouse.R;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Random;

@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class CodiciTemporaneiTest {

    @Rule
    public ActivityTestRule<CodiciTemporaneiActivity> mActivityTestRule = new ActivityTestRule<CodiciTemporaneiActivity>(CodiciTemporaneiActivity.class);

    /**
     * stage1_creazioneCodiceTemporaneo è un metodo per testare l'aggiunta di un nuovo codice temporaneo.
     */
    @Test
    public void stage1_creazioneCodiceTemporaneo() {
        onView(withId(R.id.add_fab)).perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_nome_codice),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                0)));

        String generatedString = "Test" + new Random().nextInt();
        appCompatEditText.perform(scrollTo(), replaceText(generatedString), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txt_codice_scadenza),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                2)));
        appCompatEditText2.perform(scrollTo(), click());

        ViewInteraction materialButton = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                3)));
        materialButton.perform(scrollTo(), click());

        LinearLayout mLinearLayoutSettori = getActivityInstance().findViewById(R.id.linear_layout_settori);
        CheckBox c = null;
        int count = mLinearLayoutSettori.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = mLinearLayoutSettori.getChildAt(i);
            if (v instanceof CheckBox) {
                c = (CheckBox) v;
                break;
            }
        }
        ViewInteraction checkBox = onView(
                allOf(withText(c.getText().toString()),
                        childAtPosition(
                                allOf(withId(R.id.linear_layout_settori),
                                        childAtPosition(
                                                withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                                3)),
                                0)));
        checkBox.perform(scrollTo(), click());

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.btn_codice_temporaneo), withText("Aggiungi"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                4)));
        materialButton2.perform(scrollTo(), click());
    }

    /**
     * stage2_modificaCodiceTemporaneo è un metodo per testare la modifica di un codice temporaneo.
     */
    @Test
    public void stage2_modificaCodiceTemporaneo() {
        onView(withId(R.id.rvCodiciTemporanei)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.imgEditCodice)));
        String generatedString = "Test" + new Random().nextInt();

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_nome_codice),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                0)));
        appCompatEditText.perform(scrollTo(), replaceText(generatedString), closeSoftKeyboard());


        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.btn_codice_temporaneo), withText("Applica"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                4)));
        materialButton2.perform(scrollTo(), click());
    }

    /**
     * stage2_modificaCodiceTemporaneo è un metodo per testare l'eliminazione di un codice temporaneo.
     */
    @Test
    public void stage3_eliminazioneCodiceTemporaneo() {
        onView(withId(R.id.rvCodiciTemporanei)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.imgDeleteCodice)));
        onView(withId(android.R.id.button1)).inRoot(isDialog()).perform(click());
    }
}
