# DOCUMENTAZIONE MQTT

## Hello

**Topic:** `/hello`\
\
**Body:** json

```
{"sensorsNumber":4}
```
\
**Descrizione:** topic utilizzato nel momento di inizializzazione del sistema di allarme per informare
il pannello di controllo sul numero di sensori collegati al microcontrollore;

## Movimento

**Topic:** `/movimento`\
\
**Body:** plain text
```
<codice-sensore>
```
\
**Descrizione:** topic utilizzato dal microcontrollore per segnalare un movimento proveniente da un determinato sensore

## Status

**Topic:** `/status'\
\
**Body:** json
```
{"active":["<codice-sensore>","<codice-sensore>","<codice-sensore>"]]}
```
\
**Descrizione:** topic che informa il microcontrollore di quali sono i sensori che devono essere attivati e dunque
a quali sensori si è interessati a ricevere informazioni attraverso il topic prima descritto `/movimento`